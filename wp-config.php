<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'taiheihome_db');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'root');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'root');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_IU`vg[Ly=%:7.V{(vVv{.two!U=W-+,4xg(BC: :5gm>!%_(7[JCl](DI+UCA>k');
define('SECURE_AUTH_KEY',  '%dJ.q|(mtr-RH>v@#KOvj7ml+[Q>-ara/j=i)EWNHK8^BlL.up|DYH5!UW0ed$Af');
define('LOGGED_IN_KEY',    '!8ECA`9T!8twjkbwVg3;L^o.a^uc^l%cV?t-7@mO~90{5F)>qc.+g?Gt|u3EG4KE');
define('NONCE_KEY',        '$nK(YSQEw&.wApT|c@$dMhy|<@@q8#%Rb~Lr;(6c;S)w1V6v?3<Q9QRmz*~//MLf');
define('AUTH_SALT',        'k[Y=4Je>de;_6Tb}+}oMh{:G]V^m^zu@p3*WHhn-3ojkty:OM VFD+K+{8^k)^LF');
define('SECURE_AUTH_SALT', ')uU-<tL$mmBTj,NyUBE*SdpfcPr(P{`9uC4C!$ BAvM5$AdX(OqAIU&*qWY3XO}r');
define('LOGGED_IN_SALT',   'gV16-F2p:Ct#sPdhz|sch+m+KW|u=9h<M9%EL!;YZUm-{*M_*v1?*S }72KmNqzd');
define('NONCE_SALT',       'PE^3cJz&2fm[e1v>4)gN,6E9ivwy43{[zy3deF2P3u6nH74/(]_p`;Bwju}Y?I:-');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'th_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

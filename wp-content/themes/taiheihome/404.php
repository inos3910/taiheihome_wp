<?php
/**
* ------------------------//
* fileName : 404.php
* content : 404ページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
	<div class="not_found_contents">
		<div class="page_title_wrap">
			<h1 class="not_found_title">お探しのページは<br>見つかりませんでした</h1>
		</div>
		<!-- /.page_title_wrap -->
		<?php get_template_part('block','sitemap'); ?>
	</div>
	<!-- /.not_found_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

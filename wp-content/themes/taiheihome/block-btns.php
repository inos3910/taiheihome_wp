<?php
/**
* ------------------------//
* fileName : block-product.php
* content : 各商品ページギャラリー下ボタン
* last updated : 20160427
* version : 1.0
* ------------------------//
**/
?>
<div class="product_btns">
	<a href="<?php echo home_url('/')?>product/technology/" class="btn">構造・性能を見る</a>
	<a href="<?php echo home_url('/')?>works/" class="btn">施工実績を見る</a>
	<a href="<?php echo home_url('/')?>reserve/" class="btn">来場予約フォーム</a> 
</div>
<!-- /.product_btns -->
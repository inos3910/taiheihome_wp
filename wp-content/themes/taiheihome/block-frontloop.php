<?php
/**
* ------------------------//
* fileName : block-frontloop.php
* content : フロントページNEWS・BLOGループ処理ブロック
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
?>
<?php
global $post_args;
$post_query = new WP_Query( $post_args );
if ( $post_query->have_posts() ) :
	while ( $post_query->have_posts() ) : $post_query->the_post();
          //ループ開始****************************************************
?>
<dt><?php the_time('Y.m.d'); ?></dt>
<dd><a href="<?php the_permalink();?>"><?php the_title();?></a></dd>
<?php //ループ終了************************************************
endwhile;
endif;
wp_reset_postdata();
?>
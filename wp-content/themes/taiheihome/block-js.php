<?php
/**
* ------------------------//
* fileName : block-js.php
* content : JSの読み込みをページごとに切替
* last updated : 20160416
* version : 1.0
* ------------------------//
**/
//各ページ共通 ********************************************************//　
?>
<script src="http://code.jquery.com/jquery-2.2.0.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.1.3/iscroll.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/common.js" defer></script>
<?php
//TOP **************************************************************//
if( is_front_page() ):?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/top.js" defer></script>
<?php
//about ************************************************************//
elseif( is_page('about') ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/about.js" defer></script>
<?php
//profile **********************************************************//
elseif( is_page('profile') ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/profile.js" defer></script>
<?php
//work *************************************************************//
elseif( is_page( 'works' ) ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/works.js" defer></script>
<?php
//works-post *******************************************************//
elseif( is_singular( 'works' ) ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.sliderPro.min.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/works-post.js" defer></script>
<?php
//contact **********************************************************//
elseif( is_page('contact') ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.validationEngine.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.validationEngine-ja.js" defer></script>
<script src="http://jpostal.googlecode.com/svn/trunk/jquery.jpostal.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/contact.js" defer></script>
<?php
//reserve **********************************************************//
elseif( is_page('reserve') ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.validationEngine.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.validationEngine-ja.js" defer></script>
<script src="http://jpostal.googlecode.com/svn/trunk/jquery.jpostal.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/reserve.js" defer></script>
<?php
//product detail ***************************************************//
elseif( is_page( array('classica','cafe','hobby','cube','fit') ) ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/photoswipe.min.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/photoswipe-ui-default.min.js" defer></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/product-detail.js" defer></script>
<?php
//blog&news post ***************************************************//
elseif( is_singular( array( 'post','news' ) ) ):?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/post.js" defer></script>
<?php endif;?>

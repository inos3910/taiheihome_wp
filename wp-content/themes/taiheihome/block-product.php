<?php
/**
* ------------------------//
* fileName : block-product.php
* content : 商品一覧リスト
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
?>
<?php if(is_page('product')){?>
<li class="zero"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-zero.png" height="478" width="519" alt="T's ZERO"></li>
<?php } ?>
<?php if(is_page('product') || !is_page('classica')){?>
<li class="classica">
  <a href="<?php echo home_url('/');?>product/classica/">
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-classica-bg.jpg" height="375" width="500" alt="クラシカ内観">
    <div class="item_logo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-classica.png" height="124" width="161" alt="クラシカ">
    </div>
    <!-- /.item_logo -->
  </a>
  <?php if(is_page('product')){?><p>つながる日々が、ここから。家づくりの原点と未来をみつめた注文建築です。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product') || !is_page('cafe')){?>
<li class="cafe">
  <a href="<?php echo home_url('/');?>product/cafe/">
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-cafe-bg.jpg" height="375" width="500" alt="カフェ外観">
    <div class="item_logo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cafe.png" height="125" width="162" alt="カフェ">
    </div>
    <!-- /.item_logo -->
  </a>
  <?php if(is_page('product')){?><p>やさしい時間が、続いていく。光と風に包まれるカフェスタイルの注文建築です。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product') || !is_page('hobby')){?>
<li class="hobby">
  <a href="<?php echo home_url('/');?>product/hobby/">
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-hobby-bg.jpg" height="375" width="500" alt="ホビー外観">
    <div class="item_logo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-hobby.png" height="124" width="161" alt="ホビー">
    </div>
    <!-- /.item_logo -->
  </a>
  <?php if(is_page('product')){?><p>心はずむ日々を、つくろう。趣味やこだわりに応える空間をつくる注文建築です。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product') || !is_page('cube')){?>
<li class="cube">
  <a href="<?php echo home_url('/');?>product/cube/">
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-cube-bg.jpg" height="375" width="500" alt="キューブ外観">
    <div class="item_logo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cube.png" height="124" width="161" alt="キューブ">
    </div>
    <!-- /.item_logo -->
  </a>
  <?php if(is_page('product')){?><p>のびやかな暮らし心地を叶えるCUBEフォルムの規格住宅です。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product') || !is_page('fit')){?>
<li class="fit">
  <a href="<?php echo home_url('/');?>product/fit/">
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-fit-bg.jpg" height="375" width="500" alt="フィット外観">
    <div class="item_logo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-fit.png" height="123" width="158" alt="フィット">
    </div>
    <!-- /.item_logo -->
  </a>
  <?php if(is_page('product')){?><p>私らしく、心地よく。22坪から叶えられる、個性にぴったり寄り添う規格住宅です。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product')){?>
<li class="one"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-one.png" height="331" width="540" alt="T's ONE"></li>
<?php } ?>
<?php if(is_page('product') || !is_page('technology')){?>
<li class="after">
  <a href="<?php echo home_url('/');?>product/after/">
    <span class="item_caption">アフターメンテナンス</span>
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-after-bg.jpg" height="450" width="600" alt="アフターメンテナンス">
  </a>
  <?php if(is_page('product')){?><p>24時間365日受付体制でしっかりサポート。安心で心地よい暮らしを末永く見守ります。</p><?php } ?>
</li>
<?php } ?>
<?php if(is_page('product') || !is_page('technology')){?>
<li class="reform">
  <a href="<?php echo home_url('/');?>product/reform/">
    <span class="item_caption">リフォーム</span>
    <img class="item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-reform-bg.jpg" height="450" width="599" alt="リフォーム">
  </a>
  <?php if(is_page('product')){?><p>設備交換からフルリフォームまで。暮らしの変化に応じた再生計画をご提案します。</p><?php } ?>
</li>
<?php } ?>
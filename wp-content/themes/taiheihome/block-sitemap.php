<?php
/**
* ------------------------//
* fileName : block-sitemap.php
* content : サイトマップコンテンツ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
?>
<div class="sitemap_contents_inner">
	<ul class="sitemap_list">
		<li><a href="<?php echo home_url('/')?>about/">私たちについて</a></li>
		<li class="sitemap_list_col">
			<a href="<?php echo home_url('/')?>product/">商品紹介</a>
			<ul class="sitemap_list_children">
				<li><a href="<?php echo home_url('/')?>product/classica/">T’s ZERO クラシカ</a></li>
				<li><a href="<?php echo home_url('/')?>product/cafe/">T’s ZERO +カフェ</a></li>
				<li><a href="<?php echo home_url('/')?>product/hobby/">T’s ZERO +ホビー</a></li>
				<li><a href="<?php echo home_url('/')?>product/cube/">T’s ONE キューブ</a></li>
				<li><a href="<?php echo home_url('/')?>product/fit/">T’s ONE フィット</a></li>
				<li><a href="<?php echo home_url('/')?>product/after/">アフターメンテナンス</a></li>
				<li><a href="<?php echo home_url('/')?>product/reform/">リフォーム</a></li>
			</ul>
			<!-- /.sitemap_list_children -->
			<a href="<?php echo home_url('/')?>product/technology/">構造・性能</a>
		</li>
		<!-- /.sitemap_list_col -->
		<li><a href="<?php echo home_url('/')?>works/">施工実績</a></li>
		<li><a href="<?php echo home_url('/')?>profile/">スタッフ紹介</a></li>
		<li><a href="<?php echo home_url('/')?>company/">会社概要</a></li>
		<li><a href="<?php echo home_url('/')?>news/">新着情報</a></li>
		<li><a href="<?php echo home_url('/')?>blogs/">ブログ</a></li>
		<li><a href="<?php echo home_url('/')?>reserve/">来場予約</a></li>
		<li><a href="<?php echo home_url('/')?>contact/">資料請求・お問い合わせ</a></li>
	</ul>
	<!-- /.sitemap_list -->
</div>
<!-- /.sitemap_contents_inner -->
<?php
/**
* Template Name: 送信完了
* ------------------------//
* fileName : content-complete.php
* content : 送信完了ページ
* last updated : 20160427
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
	<div class="complete_contents">
		<div class="complete_title_wrap page_title_wrap">
			<h1 class="complete_title"><?php the_title();?></h1>
		</div>
		<!-- /.complete_title_wrap.page_title_wrap -->
		<div class="complete_contents_inner">
			<div class="complete_intro">
				<p>ご入力いただいたメールアドレスに内容確認メールをお送りしました。<br>担当者より折り返しご連絡させていただきますのでしばらくお待ちください。</p>
				<p>※定休日等によりご連絡が遅くなる場合がございますがご容赦ください。</p>
				<p>※メールが届かない場合は迷惑フォルダをご確認ください。</p>
				<p>※受信拒否設定などによりメールが届かない場合がありますので設定をご確認ください。</p>
				<p>※数日たってもメールが確認できない場合はもう一度はじめからお試しください。</p>
			</div>
			<!-- /.complete_intro -->
		</div>
		<!-- /.complete_contents_inner -->
	</div>
	<!-- /.complete_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

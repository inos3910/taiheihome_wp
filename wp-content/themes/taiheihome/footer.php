<?php
/**
* ------------------------//
* fileName : footer.php
* content : フッター
* last updated : 20160415
* version : 1.0
* ------------------------//
**/
?>

<footer class="l_footer">
  <div class="footer_inner">
    <a href="#" class="footer_pagetop"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/page-top.png" height="130" width="130" alt="PAGE TOP"></a>
    <ul class="footer_navi">
      <li><a href="<?php echo home_url('/');?>about/">私たちについて</a></li>
      <li><a href="<?php echo home_url('/');?>product/">商品紹介</a></li>
      <li><a href="<?php echo home_url('/');?>works/">施工実績</a></li>
      <li><a href="<?php echo home_url('/');?>company/">会社紹介</a></li>
      <li><a href="<?php echo home_url('/');?>blogs/">ブログ</a></li>
      <li><a href="<?php echo home_url('/');?>product/after/">アフターメンテナンス</a></li>
      <li><a href="<?php echo home_url('/');?>product/reform/">リフォーム</a></li>
      <li><a href="<?php echo home_url('/');?>sitemap/">サイトマップ</a></li>
    </ul>
    <!-- /.footer_navi -->
    <div class="footer_list">
      <div class="footer_tel">
        <p><span>本社</span>埼玉県北葛飾郡杉戸町杉戸2丁目7番3号</p>
        <a href="tel:0120-38-1601"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tel-white.png" height="52" width="450" alt="0120-38-1601"></a>
      </div>
      <!-- /.footer_tel -->
      <div class="footer_link_list">
        <a class="footer_contact reserve" href="<?php echo home_url('/');?>reserve/">来場予約</a>
        <a class="footer_contact contact" href="<?php echo home_url('/');?>contact/">資料請求<span>お問い合わせ</span></a>
        <a class="footer_link" href="http://www.taiheihome.co.jp/bunjo/" target="_blank">分譲住宅情報</a>
        <a class="footer_link" href="http://www.taiheihome.co.jp/" target="_blank"><span>太平ホーム</span><span>コーポレート</span><span>サイト</span></a>
      </div>
      <!-- /.footer_link_list -->
    </div>
    <!-- /.footer_list -->
    <p class="copyright">Copyright ©TAIHEIHOME Co.,Ltd. all rights reserved.</p>
  </div>
  <!-- /.footer_inner -->
</footer>
<!--/.l_footer-->
</div>
<!--/.l_wrapper-->
<?php get_template_part('block','js'); ?>
<?php wp_footer(); ?>
</body>
</html>
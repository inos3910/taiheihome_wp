<?php
/**
* ------------------------//
* fileName : front-page.php
* content : トップページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="top_hero swiper-container">
  <ul class="hero_slide swiper-wrapper">
    <li class="swiper-slide hero_slide_01">
      <div class="hero_slide_inner">
        <a class="hero_slide_logo" href="<?php echo home_url('/');?>product/classica/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-classica.png" height="124" width="161" alt="T'sZEROクラシカ"></a>
      </div>
      <!-- /.hero_slide_inner -->
    </li>
    <li class="swiper-slide hero_slide_02">
      <div class="hero_slide_inner">
        <a class="hero_slide_logo" href="<?php echo home_url('/');?>product/cafe/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cafe.png" height="124" width="161" alt="T'sZERO+カフェ"></a>
      </div>
      <!-- /.hero_slide_inner -->
    </li>
    <li class="swiper-slide hero_slide_03">
      <div class="hero_slide_inner">
        <a class="hero_slide_logo" href="<?php echo home_url('/');?>product/hobby/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-hobby.png" height="124" width="161" alt="T'sZERO+ホビー"></a>
      </div>
      <!-- /.hero_slide_inner -->
    </li>
    <li class="swiper-slide hero_slide_04">
      <div class="hero_slide_inner">
        <a class="hero_slide_logo" href="<?php echo home_url('/');?>product/cube/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cube.png" height="124" width="161" alt="T'sONEキューブ"></a>
      </div>
      <!-- /.hero_slide_inner -->
    </li>
    <li class="swiper-slide hero_slide_05">
      <div class="hero_slide_inner">
        <a class="hero_slide_logo" href="<?php echo home_url('/');?>product/fit/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-fit.png" height="124" width="161" alt="T'sONEフィット"></a>
      </div>
      <!-- /.hero_slide_inner -->
    </li>
  </ul>
  <!-- /.hero_slide.swiper-wrapper -->
  <div class="swiper-pagination"></div>
  <div class="top_page_down">
    <div class="top_page_down_inner">
      <a href="#container"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/page-bottom.png" height="84" width="84" alt="下へ"></a>
    </div>
    <!-- /.top_page_down_inner -->
  </div>
  <!-- /.top_page_down -->
</div>
<!-- /.top_hero.swiper-container -->
<div class="l_container">
  <div class="top_feature_wrap" id="container">
    <div class="top_feature">
      <div class="top_feature_inner">
        <div class="top_feature_band">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/feature-copy.png" height="735" width="177" alt="つながる日々へ、還ろう。">
        </div>
        <!-- /.top_feature_band -->
      </div>
      <!-- /.top_feature_inner -->
      <div class="top_feature_content">
        <p>
          <span>家族と、つながる。</span>
          <span>仲間と、つながる。</span>
          <span>地域と、つながる。</span>
          <span>そんな日々の積み重ねが、</span>
          <span> かけがえのない人生を織り成していく。</span>
          <span>地域に根ざした工務店の仕事は、</span>
          <span>そのための舞台をつくることでした。</span>
          <span>どれだけ時代が変化しても、</span>
          <span>太平ホームはその原点を忘れません。</span>
          <span>さあ、いっしょに、</span>
          <span>つながる日々へと還りましょう。</span>
          <a class="top_feature_btn btn" href="<?php echo home_url('/');?>about/">太平ホームについて</a>
        </p>
      </div>
      <!-- /.feature_content -->
    </div>
    <!-- /.top_feature -->
  </div>
  <!-- /.feature_wrap -->
  <div class="top_news_container">
    <div class="top_news_inner">
      <h2 class="top_news_title top_common_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-news-event.png" height="48" width="476" alt="NEWS&amp;EVENT"><span>新着情報＆イベント</span></h2>
      <dl class="top_post_list">
        <?php
        global $post_args;
        $post_args = array( 
          'post_type' => 'news',
          'posts_per_page' => 4,
          'order' => 'DESC',
          'orderby' => 'date'
          );
        get_template_part('block','frontloop');
        ?>
      </dl>
      <!-- /.top_post_list -->
      <a href="<?php echo home_url('/');?>news/" class="news_btn btn">新着情報一覧を見る</a>
    </div>
    <!-- /.top_news_inner -->
  </div>
  <!-- /.top_news_container -->
  <div class="top_blog_container">
    <div class="top_blog_inner">
      <h2 class="top_blog_title top_common_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-blog.png" height="48" width="175" alt="BLOG"><span>ブログ</span></h2>
      <dl class="top_post_list">
        <?php
        $post_args = "";
        $post_args = array( 
          'post_type' => 'post',
          'posts_per_page' => 4,
          'order' => 'DESC',
          'orderby' => 'date'
          );
        get_template_part('block','frontloop');
        ?>
      </dl>
      <!-- /.top_post_list -->
      <a href="<?php echo home_url('/');?>blogs/" class="top_blog_btn btn">ブログ一覧を見る</a>
    </div>
    <!-- /.top_blog_inner -->
  </div>
  <!-- /.top_blog_container -->
  <div class="top_middle_visual">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/top-middle-visual.jpg" height="510" width="2171" alt="家">
  </div>
  <!-- /.top_middle_visual -->
  <div class="top_product_container">
    <div class="top_product_inner">
      <h2 class="top_product_title top_common_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="BLOG"><span>商品紹介</span></h2>
      <ul class="top_product_list">
        <li>
          <a href="<?php home_url('/')?>product/classica/">
            <img class="top_product_item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-classica-bg.jpg" height="375" width="500" alt="クラシカ">
            <div class="top_product_item_logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-classica.png" height="124" width="161" alt="クラシカ">
            </div>
            <!-- /.top_product_item_logo -->
          </a>
          <p>つながる日々が、ここから。家づくりの原点と未来をみつめた注文建築です。</p>
        </li>
        <li>
          <a href="<?php home_url('/')?>product/cafe/">
            <img class="top_product_item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-cafe-bg.jpg" height="375" width="500" alt="カフェ">
            <div class="top_product_item_logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cafe.png" height="125" width="162" alt="カフェ">
            </div>
            <!-- /.top_product_item_logo -->
          </a>
          <p>やさしい時間が、続いていく。光と風に包まれるカフェスタイルの注文建築です。</p>
        </li>
        <li>
          <a href="<?php echo home_url('/');?>product/hobby/">
            <img class="top_product_item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-hobby-bg.jpg" height="375" width="500" alt="ホビー">
            <div class="top_product_item_logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-hobby.png" height="124" width="161" alt="ホビー">
            </div>
            <!-- /.top_product_item_logo -->
          </a>
          <p>心はずむ日々を、つくろう。趣味やこだわりに応える空間をつくる注文建築です。</p>
        </li>
        <li>
          <a href="<?php echo home_url('/');?>product/cube/">
            <img class="top_product_item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-cube-bg.jpg" height="375" width="500" alt="キューブ">
            <div class="top_product_item_logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cube.png" height="124" width="161" alt="キューブ">
            </div>
            <!-- /.top_product_item_logo -->
          </a>
          <p>のびやかな暮らし心地を叶えるCUBEフォルムの規格住宅です。</p>
        </li>
        <li>
          <a href="<?php echo home_url('/');?>product/fit/">
            <img class="top_product_item_bg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-fit-bg.jpg" height="375" width="500" alt="フィット">
            <div class="top_product_item_logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-fit.png" height="123" width="158" alt="フィット">
            </div>
            <!-- /.top_product_item_logo -->
          </a>
          <p>私らしく、心地よく。22坪から叶えられる、個性にぴったり寄り添う規格住宅です。</p>
        </li>
        <li class="top_product_banner">
          <a href="<?php echo home_url('/');?>product/after/" class="after">
            <div class="caption">アフター<br>メンテナンス</div>
            <div class="pic"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-after.jpg" height="186" width="239" alt="アフターメンテナンス"></div>
          </a>
          <!-- /.after -->
          <p>24時間365日受付体制で安心を守ります。</p>
          <a href="<?php echo home_url('/');?>product/reform/" class="reform">
            <div class="caption">リフォーム</div>
            <div class="pic"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-reform.jpg" height="186" width="239" alt="リフォーム"></div>
          </a>
          <!-- /.reform -->
          <p>ニーズに応じた再生計画をご提案します。</p>
        </li>
        <!-- /.top_product_banner -->
      </ul>
      <!-- /.top_product_list -->
      <div class="top_product_btn">
        <a href="<?php echo home_url('/');?>product/" class="btn">商品一覧をみる</a>
        <a href="<?php echo home_url('/');?>works/" class="btn">施工実績一覧をみる</a>
      </div>
      <!-- /.top_product_btn -->
    </div>
    <!-- /.top_product_inner -->
  </div>
  <!-- /.top_product_container -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

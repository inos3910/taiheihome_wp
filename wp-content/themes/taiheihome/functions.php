<?php
/*-------------------------------------------*/
/*  head内不要な項目削除
/*-------------------------------------------*/
remove_action( 'wp_head', 'feed_links', 2 ); //サイト全体のフィード
remove_action( 'wp_head', 'feed_links_extra', 3 ); //その他のフィード
remove_action( 'wp_head', 'rsd_link' ); //Really Simple Discoveryリンク
remove_action( 'wp_head', 'wlwmanifest_link' ); //Windows Live Writerリンク
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); //前後の記事リンク
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); //ショートリンク
remove_action( 'wp_head', 'rel_canonical' ); //canonical属性
remove_action( 'wp_head', 'wp_generator' ); //WPバージョン
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );//絵文字のjs
remove_action( 'wp_print_styles', 'print_emoji_styles' );//絵文字のcss
//Embed系のタグを削除
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('wp_head','wp_oembed_add_host_js');

/*---------------------------------------------------------*/
/*  管理画面以外であればあらかじめ登録されているjQueryを登録解除する
/*---------------------------------------------------------*/
if(!is_admin()){
  function load_external_jQuery() {
    wp_deregister_script( 'jquery' );
  }
  add_action('wp_enqueue_scripts', 'load_external_jQuery');
}
/*-------------------------------------------*/
/*  ログイン画面ロゴ変更
/*-------------------------------------------*/
function custom_login_logo() { ?>
<style>
  .login #login h1 a {
    width: 150px;
    height: 150px;
    background: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png) no-repeat center center;
    background-size:contain;
  }
</style>
<?php }
add_action( 'login_enqueue_scripts', 'custom_login_logo' );

/*-------------------------------------------*/
/*  ログイン画面ロゴURL変更
/*-------------------------------------------*/
function login_logo_url() {
  return get_bloginfo('url');
}
add_filter('login_headerurl', 'login_logo_url');

/*-------------------------------------------*/
/*  ログイン画面ロゴtitle変更
/*-------------------------------------------*/
function login_logo_title(){
  return get_bloginfo('name');
}
add_filter('login_headertitle','login_logo_title');

/*-------------------------------------------*/
/*  フッターWordPressリンクを非表示に
/*-------------------------------------------*/
function custom_admin_footer()
{
 echo '<a href="'.home_url().'">太平ホーム</a>';
}
add_filter( 'admin_footer_text', 'custom_admin_footer' );

/*-------------------------------------------*/
/*  アイキャッチ画像設定
/*-------------------------------------------*/
add_theme_support('post-thumbnails');
update_option( 'medium_crop',true ); //中サイズ時のトリミング機能を有効
update_option( 'large_crop',true ); //大サイズ時のトリミング機能を有効

/*-------------------------------------------*/
/*  投稿画面の不要な項目を削除
/*-------------------------------------------*/
function remove_post_metaboxes() {
    //remove_meta_box('postcustom', 'post', 'normal'); // カスタムフィールド
    remove_meta_box('postexcerpt', 'post', 'normal'); // 抜粋
    remove_meta_box('commentstatusdiv', 'post', 'normal'); // コメント設定
    remove_meta_box('trackbacksdiv', 'post', 'normal'); // トラックバック設定
    remove_meta_box('revisionsdiv', 'post', 'normal'); // リビジョン表示
    remove_meta_box('formatdiv', 'post', 'normal'); // フォーマット設定
    remove_meta_box('slugdiv', 'post', 'normal'); // スラッグ設定
    //remove_meta_box('authordiv', 'post', 'normal'); // 投稿者
    //remove_meta_box('categorydiv', 'post', 'normal'); // カテゴリー
    //remove_meta_box('tagsdiv-post_tag', 'post', 'normal'); // タグ
  }
  add_action('admin_menu', 'remove_post_metaboxes');

  /*-------------------------------------------*/
/*  ビジュアルエディタの自動整形設定
/*-------------------------------------------*/
// オートフォーマット関連の無効化
add_action('init', function() {
  remove_filter('the_title', 'wptexturize');
  remove_filter('the_content', 'wptexturize');
  remove_filter('the_excerpt', 'wptexturize');
  remove_filter('the_title', 'wpautop');
  remove_filter('the_content', 'wpautop');
  remove_filter('the_excerpt', 'wpautop');
  remove_filter('the_editor_content', 'wp_richedit_pre');
});

// オートフォーマット関連の無効化 TinyMCE
add_filter('tiny_mce_before_init', function($init) {
  $init['wpautop'] = false;
  $init['apply_source_formatting'] = true;
  return $init;
});

/*-------------------------------------------*/
/*  オートセーブ無効
/*-------------------------------------------*/
function disable_autosave() {
  wp_deregister_script('autosave');
}
add_action('wp_print_scripts','disable_autosave');


/*-------------------------------------------*/
/*  サイト表示時管理バーの削除
/*-------------------------------------------*/
add_filter('show_admin_bar', '__return_false');

/*-------------------------------------------*/
/*  管理バー不要な項目の削除
/*-------------------------------------------*/
function remove_admin_bar_menu( $wp_admin_bar ) {
    $wp_admin_bar->remove_menu('wp-logo'); // WordPressロゴ
    //$wp_admin_bar->remove_menu('my-sites'); // 参加サイト for マルチサイト
    //$wp_admin_bar->remove_menu('site-name'); // サイト名
    //$wp_admin_bar->remove_menu('view-site'); // サイト名 -> サイトを表示
    $wp_admin_bar->remove_menu('updates'); // 更新
    $wp_admin_bar->remove_menu('comments'); // コメント
    $wp_admin_bar->remove_menu('new-content'); // 新規
    $wp_admin_bar->remove_menu('new-post'); // 新規 -> 投稿
    $wp_admin_bar->remove_menu('new-media'); // 新規 -> メディア
    $wp_admin_bar->remove_menu('new-link'); // 新規 -> リンク
    $wp_admin_bar->remove_menu('new-page'); // 新規 -> 固定ページ
    $wp_admin_bar->remove_menu('new-user'); // 新規 -> ユーザー
    //$wp_admin_bar->remove_menu('my-account'); // マイアカウント
    //$wp_admin_bar->remove_menu('user-info'); // マイアカウント -> プロフィール
    //$wp_admin_bar->remove_menu('edit-profile'); // マイアカウント -> プロフィール編集
    //$wp_admin_bar->remove_menu('logout'); // マイアカウント -> ログアウト
    //$wp_admin_bar->remove_menu('search'); // 検索
  }
  add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 201 );

  /*-------------------------------------------*/
/*  管理バーにログアウトを追加
/*-------------------------------------------*/
function add_new_item_in_admin_bar() {
 global $wp_admin_bar;
 $wp_admin_bar->add_menu(array(
   'id' => 'new_item_in_admin_bar',
   'title' => __('ログアウト'),
   'href' => wp_logout_url()
   ));
}
add_action('wp_before_admin_bar_render', 'add_new_item_in_admin_bar');


/*-------------------------------------------*/
/*  管理画面メニューの【投稿】のラベルを変更
/*-------------------------------------------*/
function revcon_change_post_label() {
  global $menu;
  global $submenu;
  $menu[5][0]                   = 'ブログ';
  $submenu['edit.php'][5][0]    = 'ブログ一覧';
  $submenu['edit.php'][10][0]   = '新規追加';
  $submenu['edit.php'][16][0]   = 'タグ';
  echo '';
}
function revcon_change_post_object() {
  global $wp_post_types;
  $labels                       = &$wp_post_types['post']->labels;
  $labels->name                 = 'ブログ';
  $labels->singular_name        = 'ブログ';
  $labels->add_new              = '新規追加';
  $labels->add_new_item         = 'ブログを追加';
  $labels->edit_item            = 'ブログの編集';
  $labels->new_item             = 'ブログ';
  $labels->view_item            = 'ブログの表示';
  $labels->search_items         = 'ブログを検索';
  $labels->not_found            = 'ブログが見つかりませんでした。';
  $labels->not_found_in_trash   = 'ゴミ箱内にブログが見つかりませんでした。';
  $labels->all_items            = '全てのブログ';
  $labels->menu_name            = 'ブログ';
  $labels->name_admin_bar       = 'ブログ';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

/*-------------------------------------------*/
/*  新着情報カスタム投稿タイプを作成
/*-------------------------------------------*/
function news_custom_post_type(){
  $labels = array(
    'name'               => '新着情報',
    'singular_name'      => '新着情報',
    'add_new'            => '新規追加',
    'add_new_item'       => '新規項目追加',
    'edit_item'          => '項目を編集',
    'new_item'           => '新規項目',
    'view_item'          => '項目を表示',
    'search_items'       => '項目検索',
    'not_found'          =>  '記事が見つかりません',
    'not_found_in_trash' => 'ゴミ箱に記事はありません',
    'parent_item_colon'  => ''
    );
  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'query_var'           => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'hierarchical'        => false,
    'menu_position'       => 5,
    'supports'            => array('title','editor','thumbnail')
    );
  register_post_type('news',$args);
}
add_action('init', 'news_custom_post_type');

/*-------------------------------------------*/
/*  施工実績カスタム投稿タイプを作成
/*-------------------------------------------*/
function works_custom_post_type(){
  $labels = array(
    'name'               => '施工実績',
    'singular_name'      => '施工実績',
    'add_new'            => '新規追加',
    'add_new_item'       => '新規項目追加',
    'edit_item'          => '項目を編集',
    'new_item'           => '新規項目',
    'view_item'          => '項目を表示',
    'search_items'       => '項目検索',
    'not_found'          =>  '記事が見つかりません',
    'not_found_in_trash' => 'ゴミ箱に記事はありません',
    'parent_item_colon'  => ''
    );
  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'query_var'           => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'hierarchical'        => false,
    'menu_position'       => 5,
    'supports'            => array('title','editor','thumbnail')
    );
  register_post_type('works',$args);
}
add_action('init', 'works_custom_post_type');

/*-------------------------------------------*/
/*  投稿一覧の不要な項目の削除
/*-------------------------------------------*/
function post_custom_columns ($columns) {
    //unset($columns['cb']); // チェックボックス
    //unset($columns['title']); // タイトル
    unset($columns['author']); // 作成者
    //unset($columns['categories']); // カテゴリー
    unset($columns['tags']); // タグ、カスタムフィールド
    unset($columns['comments']); // コメント
    //unset($columns['date']); // 日付
    return $columns;
  }
  add_filter('manage_posts_columns', 'post_custom_columns');

  /*-------------------------------------------*/
/*  固定ページ一覧の不要な項目の削除
/*-------------------------------------------*/
function page_custom_columns ($columns) {
    //unset($columns['cb']); // チェックボックス
    //unset($columns['title']); // タイトル
    //unset($columns['author']); // 作成者
    //unset($columns['categories']); // カテゴリー
    unset($columns['tags']); // タグ、カスタムフィールド
    unset($columns['comments']); // コメント
    //unset($columns['date']); // 日付
    return $columns;
  }
  add_filter('manage_pages_columns', 'page_custom_columns');

  /*-------------------------------------------*/
/* 投稿者・寄稿者・購読者の場合のメニュー表示設定
/*-------------------------------------------*/
function remove_menus () {
 if (current_user_can('author')) { 
   global $menu;
 unset($menu[2]); // ダッシュボード
 unset($menu[4]); // メニューの線1
 //unset($menu[5]); // 投稿
 //unset($menu[10]); // メディア
 unset($menu[15]); // リンク
 unset($menu[20]); // ページ
 unset($menu[25]); // コメント
 unset($menu[59]); // メニューの線2
 unset($menu[60]); // テーマ
 unset($menu[65]); // プラグイン
 unset($menu[70]); // プロフィール
 unset($menu[75]); // ツール
 unset($menu[80]); // 設定
 unset($menu[90]); // メニューの線3
} elseif(current_user_can('editor')){
 global $menu;
 unset($menu[2]); // ダッシュボード
 unset($menu[4]); // メニューの線1
 //unset($menu[5]); // 投稿
 //unset($menu[10]); // メディア
 unset($menu[15]); // リンク
 unset($menu[20]); // ページ
 unset($menu[25]); // コメント
 unset($menu[59]); // メニューの線2
 unset($menu[60]); // テーマ
 unset($menu[65]); // プラグイン
 unset($menu[70]); // プロフィール
 unset($menu[75]); // ツール
 //unset($menu[80]); // 設定
 unset($menu[90]); // メニューの線3
} elseif(current_user_can('subscriber') || current_user_can('contributor')){
 remove_menu_page('edit.php?post_type=info'); // カスタム投稿「お知らせ」の非表示
 global $menu;
 unset($menu[2]); // ダッシュボード
 unset($menu[4]); // メニューの線1
 unset($menu[5]); // ブログ
 unset($menu[10]); // メディア
 unset($menu[15]); // リンク
 unset($menu[20]); // ページ
 unset($menu[25]); // コメント
 unset($menu[59]); // メニューの線2
 unset($menu[60]); // テーマ
 unset($menu[65]); // プラグイン
 unset($menu[70]); // プロフィール
 unset($menu[75]); // ツール
 unset($menu[80]); // 設定
 unset($menu[90]); // メニューの線3
}
}
add_action('admin_menu', 'remove_menus');

/*-----------------------------------------------------*/
/* 投稿者・寄稿者・購読者の場合ダッシュボードウィジェット非表示
/*-----------------------------------------------------*/
function example_remove_dashboard_widgets() {
 if (current_user_can('author') || current_user_can('subscriber') || current_user_can('contributor')) { 
   global $wp_meta_boxes;
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

/*---------------------------------------------------*/
/*  ブログdivタグに付与される横幅のスタイルを削除する
/*---------------------------------------------------*/
add_shortcode('caption', 'my_img_caption_shortcode');
function my_img_caption_shortcode($attr, $content = null) {
	if ( ! isset( $attr['caption'] ) ) {
		if ( preg_match( '#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches ) ) {
			$content = $matches[1];
			$attr['caption'] = trim( $matches[2] );
		}
	}

	$output = apply_filters('img_caption_shortcode', '', $attr, $content);
	if ( $output != '' )
		return $output;

	extract(shortcode_atts(array(
		'id'	=> '',
		'align'	=> 'alignnone',
		'width'	=> '',
		'caption' => ''
   ), $attr, 'caption'));

	if ( 1 > (int) $width || empty($caption) )
		return $content;
	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
	return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '">' . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';

}

/*---------------------------------------------------*/
/*  投稿画面のカテゴリ新規追加とよく使うものを削除
/*---------------------------------------------------*/
function hide_category_add() {
 global $pagenow;
   global $post_type;//投稿タイプで切り分けたいときに使う
   if (is_admin() && ($pagenow=='post-new.php' || $pagenow=='post.php') && $post_type=="post"){
     echo '<style type="text/css">
       #category-adder{display:none;}
       #category-tabs li.hide-if-no-js{display:none;}
   </style>';
 }
}
add_action( 'admin_head', 'hide_category_add'  );

/*-------------------------------------------*/
/*  description抜粋文字
/*-------------------------------------------*/
function ltl_get_the_excerpt($post_id='', $length=120){
  global $post; $post_bu = '';

  if(!$post_id){
    $post_id = get_the_ID();
  } else {
    $post_bu = $post;
    $post = get_post($post_id);
  }
  $mojionly = strip_tags($post->post_content);
  $mojionly = str_replace('&nbsp;', '', $mojionly);
  if(mb_strlen($mojionly ) > $length) $t = '...';
  $content =  mb_substr($mojionly, 0, $length);
  $content .= $t;
  if($post_bu) $post = $post_bu;
  return $content;
}
/*-------------------------------------------*/
/*  ディスクリプション
/*-------------------------------------------*/
function get_description() {
  global $post;
  $post_id = get_the_ID();
  $description = get_post_meta($post_id , 'cf_description' ,true);
  if(is_page()){
    if($description){ 
      //固定ページディスクリプション設定がある場合
      $description = html_entity_decode(strip_tags($description));
      $description = str_replace(array("\r\n","\n","\r","&nbsp;"), "", $description);
      return $description;
    } else { 
      //ない場合は本文の抜粋
      $str = ltl_get_the_excerpt();
      $str = str_replace(array("\r\n","\n","\r"), "", $str);
      return $str;
    }
  } else if(is_singular()){
    if($description){
      //投稿ページディスクリプション設定がある場合
      $description = html_entity_decode(strip_tags($description));
      $description = str_replace(array("\r\n","\n","\r","&nbsp;"), "", $description);
      return $description;
    } else {
      //ない場合は本文の抜粋
      $str = ltl_get_the_excerpt();
      $str = str_replace(array("\r\n","\n","\r"), "", $str);
      return $str;
    }
  }
}
/*-------------------------------------------*/
/*  タイトルタグ
/*-------------------------------------------*/
function get_title_tag() {
  global $post;
  $post_id = get_the_ID();
  if(is_category()){
    $cat_title = single_cat_title('',false);
    echo $cat_title.' | '.get_bloginfo('name');
  } else {
    $title = get_post_meta($post_id , 'cf_title' ,true);
    if($title){
      echo $title;
    } else {
      wp_title('|', true, 'right');
      bloginfo('name');
    }
  }
}
/*-------------------------------------------*/
/*  アイキャッチ画像url生成
/*-------------------------------------------*/
function eye_img($size){
  // アイキャッチ画像のIDを取得
  $thumbnail_id = get_post_thumbnail_id();
  $eye_img = wp_get_attachment_image_src( $thumbnail_id , $size );
  return $eye_img[0];
}
/*-------------------------------------------*/
/*  抜粋
/*-------------------------------------------*/
function new_excerpt_mblength($length) {
 return 100;
} 
add_filter('excerpt_mblength', 'new_excerpt_mblength');

function new_excerpt_more($more) {
  return '…';
} 
add_filter('excerpt_more', 'new_excerpt_more');

/*----------------------------------------------------*/
/*  Ajax header要素内に JavaScriptのグローバル変数を出力
/*----------------------------------------------------*/
function add_my_ajaxurl() {
  if ( is_page('works') ) {
    ?>
    <script>
      var ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
    </script>
    <?php
  }
}
add_action( 'wp_head', 'add_my_ajaxurl', 1 );

/*----------------------------------------------------*/
/*  JavaScript から呼び出す PHP 関数 works_post
/*  さらに読み込むボタンクリックで呼び出す関数
/*----------------------------------------------------*/
function works_post(){
  global $post;
  $exclude = array();
  $ex = $_POST['exclude'];
  $ex = array_merge($exclude,$ex);
  $count = 0;
  $args = array(
    'post_type' => 'works',
    'posts_per_page' => 6,
    'orderby' => 'rand',
    'post__not_in' => $ex
    );
  $postslist = new WP_Query( $args );
  $html = '';
  if ( $postslist->have_posts() ) :
    while ( $postslist->have_posts() ) : $postslist->the_post();
  $exclude[] = $post->ID;
  $works_title = get_the_title();
  $works_image = get_post_meta($post->ID, 'cf_works_slide', true);
  $html.= '<li class="add" data-ex="'.get_the_ID().'">';
  $html.= '<a href="'.get_the_permalink().'">'.wp_get_attachment_image($works_image, 'medium').'</a>';
  $html.= '<p>'.$works_title.'</p>';
  $html.= '</li>';
  endwhile;
  endif;
  wp_reset_postdata();
  //JSONデータを取得して出力
  echo json_encode($html);
  //終了
  exit;
}
add_action( 'wp_ajax_works_post', 'works_post' );
add_action( 'wp_ajax_nopriv_works_post', 'works_post' );

/*----------------------------------------------------*/
/*  エスケープ関数
/*----------------------------------------------------*/
function h($str) {
  return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
}
/*----------------------------------------------------*/
/*  wp_mail() メールヘッダー追加
/*----------------------------------------------------*/
/* サイト管理者メールアドレス取得 */
function set_mail_from( $email ) {
  //メールアドレス
  $admin_info  = get_userdata(1);
  $admin_email = $admin_info->user_email;
  $admin_email = str_replace("\0", "", $admin_email);
  $admin_email = htmlspecialchars($admin_email, ENT_QUOTES, 'UTF-8');
  return $admin_email;
}
function set_mail_from_name( $email_from ) {
  //送信者名
  return '太平ホーム';
}
function set_return_path( $phpmailer ) {
  //Retern-Path
  $admin_info  = get_userdata(1);
  $admin_email = $admin_info->user_email;
  $admin_email = str_replace("\0", "", $admin_email);
  $admin_email = htmlspecialchars($admin_email, ENT_QUOTES, 'UTF-8');
  $phpmailer->Sender = $admin_email;
}
add_filter( 'wp_mail_from', 'set_mail_from' );
add_filter( 'wp_mail_from_name', 'set_mail_from_name' );
add_action( 'phpmailer_init', 'set_return_path' );
?>

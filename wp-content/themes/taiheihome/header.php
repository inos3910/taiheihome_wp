<?php
/**
* ------------------------//
* fileName : header.php
* content : ヘッダー
* last updated : 20160419
* version : 1.0
* ------------------------//
**/
//canonical
global $page, $paged, $wp_query;
// ホーム
if(is_front_page()){
  $canonical_url = home_url();
// カテゴリーページ
}else if (is_category()){
  $canonical_url = get_category_link(get_query_var('cat'));
// 固定ページ＆個別記事
} else if (is_page()||is_single()) {
  $canonical_url = get_permalink();
// その他
} else {
  $canonical_url = null;
}
//キーワード
$keywords = get_post_meta($post->ID , 'cf_keyword' ,true);
//noindexチェック
$noindex = get_post_meta($post->ID , 'cf_noindex' ,true);

//ページごとに付与するclass
if( is_front_page() ){
  $bodyClass = null;
}
elseif(is_page()){
  $bodyClass = esc_attr( $post->post_name ).'_page';
} elseif (is_404()){
  $bodyClass = 'not_found';
} else {
  $bodyClass = null;
}
?>
<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<?php if( $noindex || is_404() || is_paged() ){?><meta name="robots" content="noindex,nofollow" /><?php }?>
<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
<title><?php get_title_tag();?></title>
<?php if( get_description() ){?><meta name="description" content="<?php echo get_description();?>" /><?php } ?>
<?php if( $keywords ){ ?><meta name="keywords" content="<?php echo $keywords; ?>" /><?php } ?>
<?php if ($canonical_url == !null){ ?><link rel="canonical" href="<?php echo $canonical_url; ?>" /><?php } ?>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/clip_icon.png">
<link rel="alternate" type="application/rss+xml" href="<?php bloginfo('rss2_url'); ?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style.css">
</head>
<?php wp_head(); ?>
<body<?php if($bodyClass){?> class="<?php echo $bodyClass; ?>"<?php } ?>>
<div class="l_wrapper">
  <header class="l_header">
    <div class="header_inner">
      <?php if( is_front_page() ): ?>
        <h1 class="header_logo header_top_logo"><a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" height="285" width="182" alt="いつかは夢の家づくり 太平ホーム"></a></h1>
      <?php else: ?>
        <div class="header_logo"><a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" height="285" width="182" alt="いつかは夢の家づくり 太平ホーム"></a></div>
      <?php endif; ?>
      <div class="header_navi_area">
        <nav class="header_gnavi">
          <ul class="header_gnavi_list">
            <li<?php if( is_page('about') ){?> class="active"<?php } ?>><a href="<?php echo home_url('/');?>about/">私たちについて</a></li>
            <li<?php if( is_page(array('product','classica','cafe','hobby','cube','fit','after','reform') ) ){?> class="active"<?php } ?>>
            <a href="<?php echo home_url('/');?>product/">商品紹介</a>
            <div class="header_gnavi_child header_gnavi_child_01<?php if( is_page( array('product','classica','cafe','hobby','cube','fit','after','reform') ) ){?> current<?php } ?>">
              <a<?php if( is_page('classica') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/classica/">T'sZEROクラシカ</a>
              <a<?php if( is_page('cafe') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/cafe/">T'sZERO+カフェ</a>
              <a<?php if( is_page('hobby') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/hobby/">T'sZERO+ホビー</a>
              <a<?php if( is_page('cube') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/cube/">T'sONEキューブ</a>
              <a<?php if( is_page('fit') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/fit/">T'sONEフィット</a>
              <a<?php if( is_page('after') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/after/">アフターメンテナンス</a>
              <a<?php if( is_page('reform') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>product/reform/">リフォーム</a>
            </div>
            <!-- /.header_gnavi_child -->
          </li>
          <li<?php if( is_page('works') ){?> class="active"<?php } ?>><a href="<?php echo home_url('/');?>works/">施工実績</a></li>
          <li<?php if( is_page(array('company','profile')) ){?> class="active"<?php } ?>>
          <a href="<?php echo home_url('/');?>profile/"<?php if( is_page('company') ){?> class="active"<?php } ?>>会社紹介</a>
          <div class="header_gnavi_child header_gnavi_child_02<?php if( is_page( array('profile','company') ) ){?> current<?php } ?>">
            <a<?php if( is_page('profile') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>profile/">スタッフ紹介</a>
            <a<?php if( is_page('company') ){?> class="active"<?php } ?> href="<?php echo home_url('/');?>company/">会社概要</a>
          </div>
          <!-- /.header_gnavi_child -->
        </li>
        <li<?php if( is_page('blog') ){?> class="active"<?php } ?>><a href="<?php echo home_url('/');?>blogs/">ブログ</a></li>
      </ul>
      <!-- /.header_gnavi_list -->
    </nav>
    <!-- /.header_gnavi -->
    <div class="header_links">
      <div class="header_contact">
        <a class="reserve" href="<?php echo home_url('/');?>reserve/">来場予約</a>
        <a class="contact"href="<?php echo home_url('/');?>contact/">資料請求・<span>お問い合わせ</span></a>
      </div>
      <!-- /.header_contact -->
      <div class="header_aside">
        <div class="header_external">
          <a href="http://www.taiheihome.co.jp/bunjo/" target="_blank">分譲住宅情報</a>
          <a href="http://www.taiheihome.co.jp/" target="_blank">太平ホームコーポレートサイト</a>
        </div>
        <!-- /.header_external -->
        <div class="header_tel">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tel-black.png" height="43" width="395" alt="0120-38-1601">
        </div>
      </div>
      <!-- /.header_aside -->
    </div>
    <!-- /.header_links -->
  </div>
  <!-- /.header_navi_area -->
</div>
<!-- /.header_inner -->
<div class="header_tel_btn">
  <a href="tel:0120-38-1601"><i class="icon-phone"></i></a>
</div>
<!-- /.header_tel_btn -->
<div class="header_sp_nav">
  <nav>
    <ul class="header_sp_nav_list">
      <li class="logo"><a href="<?php echo home_url('/');?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" height="285" width="182" alt="いつかは夢の家づくり 太平ホーム"></a></li>
      <li class="reserve"><a href="<?php echo home_url('/');?>reserve/" class="sp_nav_btn">来場予約</a></li>
      <li class="contact"><a href="<?php echo home_url('/');?>contact/" class="sp_nav_btn">資料請求・お問い合わせ</a></li>
      <li><a href="<?php echo home_url('/');?>about/">私たちについて</a></li>
      <li>
        <a href="<?php echo home_url('/');?>product/">商品紹介</a>
        <ul class="header_sp_nav_child">
          <li><a href="<?php echo home_url('/');?>product/classica/">T'sZEROクラシカ</a></li>
          <li><a href="<?php echo home_url('/');?>product/cafe/">T'sZERO+カフェ</a></li>
          <li><a href="<?php echo home_url('/');?>product/hobby/">T'sZERO+ホビー</a></li>
          <li><a href="<?php echo home_url('/');?>product/cube/">T'sONEキューブ</a></li>
          <li><a href="<?php echo home_url('/');?>product/fit/">T'sONEフィット</a></li>
          <li><a href="<?php echo home_url('/');?>product/after/">アフターメンテナンス</a></li>
          <li><a href="<?php echo home_url('/');?>product/reform/">リフォーム</a></li>
        </ul>
        <!-- /.header_sp_nav_child -->
      </li>
      <li><a href="<?php echo home_url('/');?>works/">施工実績</a></li>
      <li>
        <a href="<?php echo home_url('/');?>profile/">会社紹介</a>
        <ul class="header_sp_nav_child">
          <li><a href="<?php echo home_url('/');?>profile/">スタッフ紹介</a></li>
          <li><a href="<?php echo home_url('/');?>company/">会社概要</a></li>
        </ul>
        <!-- /.header_sp_nav_child -->
      </li>
      <li><a href="<?php echo home_url('/');?>blogs/">ブログ</a></li>
      <li><a href="http://www.taiheihome.co.jp/bunjo/" target="_blank">分譲住宅情報</a></li>
      <li><a href="http://www.taiheihome.co.jp/" target="_blank">太平ホームコーポレートサイト</a></li>
    </ul>
    <!-- /.header_sp_nav_list -->
  </nav>
  <div class="header_menu_btn">
    <ul>
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div>
  <!-- /.header_menu_btn -->
</div>
<!-- /.header_sp_nav -->
</header>
<!--/.l_header-->
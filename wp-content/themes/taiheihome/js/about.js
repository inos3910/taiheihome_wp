(function($){
	"use strict";
	//変数 --------------------------//
	var $win  = $(window);
	var $doc  = $(document);
	var $winW = $win.innerWidth();
	//スクロールエフェクト-----------------------------//
	var $target = $('.about_parallax');
	$win.on('load scroll',function (){
		var scrollTop     = $(this).scrollTop();
		$target.each(function(i,elem){
			var offsetTop = $(elem).offset().top;
			var scrVal    = scrollTop - offsetTop;
			var targetH   = $(elem).find('.about_parallax_box').innerHeight();
			var scrBoxVal = 50+(scrVal/targetH*100)/3;
			scrVal        = (scrVal/1.5).toFixed(6);
			if(scrollTop >= offsetTop){
				if(i===0 && $winW < 768){
					//
				} else {
					$(elem).css({
						'transform': 'translate3d(0,'+ scrVal +'px,0)'
					});
					$(elem).find('.about_parallax_box').css({
						'transform': 'translate3d(0,'+ -scrBoxVal +'%,0)'
					});
				}
			} else {
				$(elem).css({
					'transform': 'translate3d(0,0,0)'
				});
				$(elem).find('.about_parallax_box').css({
					'transform': 'translateY(0,-50%,0)'
				});
			}
		});
	});
})(jQuery);
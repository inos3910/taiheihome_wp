(function($){
	"use strict";
	//グローバル変数 ---------------------//
	var $win = $(window);
	var $doc = $(document);
	var winW = $win.innerWidth();
	var winH = $win.innerHeight();
	var $this;
	//スマホタッチイベント制御 ------------//
	function spTouch(func){
		if ('touchstart' === event.type){
			$this.attr('data-touchstarted', '');
			return;
		}
		if ('touchmove' === event.type){
			$this.removeAttr('data-touchstarted');
			return;
		}
		if ('undefined' !== typeof $this.attr('data-touchstarted')){
			func.call($this, event);
			$this.removeAttr('data-touchstarted');
		}
	}
	//ドロワーメニュ ---------------------//
	/* オーバーレイ要素の追加 */
	var $html   = $('html');
	$html.append($('<div>').addClass('overlay'));
	var $overlay = $('.overlay');
	var $spNavi = $('.header_sp_nav');
	var $menuBtn = $('.header_menu_btn');
	$doc.on('touchstart touchmove touchend', '.header_menu_btn', function(){
		$this = $(this);
		spTouch(menuBtnTapped);
	});
	$doc.on('click', '.header_menu_btn', function(){
		$this = $(this);
		menuBtnTapped.call($this, event);
	});
	function menuBtnTapped(event){
		event.preventDefault();
		event.stopPropagation();
		$overlay.toggleClass('active')
		$this.toggleClass('open');
		$spNavi.toggleClass('active');
		if($this.hasClass('open')){
			// スクロール無効
			$doc.on('touchmove.noScroll', function(e) {
				e.preventDefault();
			});
		} else {
			// スクロール無効 解除
			$doc.off('.noScroll');
		}
	}
	$doc.on('touchstart touchmove touchend','.overlay', function(event){
		$this = $(this);
		spTouch(closeTapped);
	});
	$doc.on('click', '.overlay', function(){
		$this = $(this);
		closeTapped.call($this, event);
	});
	function closeTapped(event){
		event.preventDefault();
		event.stopPropagation();
		$this.removeClass('active');
		$spNavi.removeClass('active');
		$menuBtn.removeClass('open');
		var $confirm = $('.confirm');
		if($confirm){
			$confirm.velocity('fadeOut', { duration: 300, easing: 'easeOutQuad' });
		}
		// スクロール無効 解除
		$doc.off('.noScroll');
	}

	//iScroll --------------------------//
	var myScroll = new IScroll('.header_sp_nav', {
		probeType: 3,
		mouseWheel: true,
		click: true
	});

	//PC ナビhover ---------------------//
	var $gnaviItem  = $('.header_gnavi_list li');
	var $gnaviChild = $('.header_gnavi_child');
	$gnaviItem.on({
		'mouseenter':function(){
			$gnaviChild.css({
				visibility: 'hidden',
				opacity: 0,
			});
			$(this).find('.header_gnavi_child').css({
				visibility: 'visible',
				opacity: 1,
			});
		},
		'mouseleave':function(){
			$gnaviChild.removeAttr('style');
			$(this).find('.header_gnavi_child').removeAttr('style');
		}
	});
	//スムーススクロール ------------------//
	var $ancer = $('a[href^="#"]');
	$ancer.on('click',function () {
		$this = $(this);
		var href = $this.attr('href');
		var $target = $(href === '#' || href === '' ? 'html,body' : href);
		$target.velocity('scroll', { duration: 600, easing: 'easeOutCirc' });
		return false;
	});
	function animateScroll(event){
		var href = $this.attr('href');
		var $target = $(href === '#' || href === '' ? 'html,body' : href);
		$target.velocity('scroll', { duration: 600, easing: 'easeOutCirc' });
		return false;
	}
	//PCでtelリンク無効 --------------------// 
	var ua = navigator.userAgent.toLowerCase();
	var isMobile = /iphone/.test(ua)||/android(.+)?mobile/.test(ua);

	if (!isMobile) {
		$('a[href^="tel:"]').on('click', function(e) {
			e.preventDefault();
		});
	}
})(jQuery);
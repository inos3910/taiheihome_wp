(function($){
	"use strict";
	var $doc    = $(document);
	var $all    = $('html');
	var $body   = $('body');
	var _confirm = '<div class="confirm"><dl><dt class="label">ご希望の資料</dt><dd class="confirm_document"></dd><dt class="label">お問い合わせ内容</dt><dd class="confirm_type"></dd><dt class="label">その他のお問い合わせ内容</dt><dd class="confirm_othertype"></dd><dt class="label">お名前</dt><dd class="confirm_name"></dd><dt class="label">ふりがな</dt><dd class="confirm_kana"></dd><dt class="label">メールアドレス</dt><dd class="confirm_email"></dd><dt class="label">ご住所</dt><dd class="confirm_address"></dd><dt class="label">TEL</dt><dd class="confirm_tel"></dd></dl><div class="btns_wrap"><div class="btn"><input type="submit" id="send_btn" value="この内容で送信する"></div><button class="btn back">戻る</button></div></div>';
	$body.append(_confirm);
	var $confirm = $('.confirm');
	var $overlay = $('.overlay');
	//郵便番号自動補完 --------------------------------//
	var $postalCode = $('#postal-code');
	$postalCode.jpostal({
		postcode : [
		'#postal-code',
		],
		address : {
			'#user_address'  : '%3%4%5%6%7',
		}
	});
	//validation ------------------------------------//
	var $form         = $('#contact_form');
	var $check        = $('.contact_check');
	var $contactChk01 = $('#contact_check_01');
	var $contactChk02 = $('#contact_check_02');
	var $message      = $('#contact_message');
	var $userName     = $('#user_name');
	var $userNameKana = $('#user_name_kana');
	var $userEmail    = $('#user_email');
	var $userAddress  = $('#user_address');
	var $userTel      = $('#user_tel');

	$check.on('load change',function(){
		var checked_01 = $contactChk01.prop('checked');
		var checked_02 = $contactChk02.prop('checked');
		if(!checked_01){
			$contactChk02.prop('disabled',true);
		} else if(!checked_02){
			$contactChk01.prop('disabled',true);
		} else {
			$check.prop('disabled',false);
		}
	}).change();

	$message.addClass('validate[maxSize[1000]]');
	$userName.addClass('validate[required,maxSize[30]]');
	$userNameKana.addClass('validate[required,custom[kana],maxSize[30]]');
	$userEmail.addClass('validate[required,custom[email],maxSize[90]]');
	$postalCode.addClass('validate[required,custom[postal],maxSize[10]]');
	$userAddress.addClass('validate[required,maxSize[150]]');
	$userTel.addClass('validate[required,custom[phone],maxSize[15]]');
	$form.validationEngine('attach', {
		promptPosition:"inline"
	});
	//確認画面表示 -----------------------//
	var $checked01    = $('#contact_check_01');
	var $checked02    = $('#contact_check_02');
	var $type         = $('#contact_type');
	var $other        = $('#contact_message');
	var $confDoc      = $('.confirm_document');
	var $confType     = $('.confirm_type');
	var $confOther    = $('.confirm_othertype');
	var $confName     = $('.confirm_name');
	var $confKana     = $('.confirm_kana');
	var $confEmail    = $('.confirm_email');
	var $confAdress   = $('.confirm_address');
	var $confTel      = $('.confirm_tel');
	$doc.on('click','#confirm_btn',function(e){
		e.preventDefault();
		if($form.validationEngine('validate')　===　true){

			var checkedVal01;
			if($checked01.prop('checked') === true){
				checkedVal01 = $checked01.val();
			} else {
				checkedVal01 = "";
			}

			var checkedVal02;
			if($checked02.prop('checked') === true){
				checkedVal02 = $checked02.val();
			} else {
				checkedVal02 = "";
			}

			$confDoc.text(checkedVal01+' '+checkedVal02);
			$confType.text($type.val());
			$confOther.text($other.val());
			$confName.text($userName.val());
			$confKana.text($userNameKana.val());
			$confEmail.text($userEmail.val());
			$confAdress.text($postalCode.val()+' '+$userAddress.val());
			$confTel.text($userTel.val());
			$confirm.velocity('fadeIn', {
				duration: 300,
				easing: 'easeOutQuad',
				begin: function() {
					modalOn();
				}
			});
		}
	});
	
	//戻るボタン ------------------------//
	$doc.on('click','.back',function(e){
		e.preventDefault();
		var $confirmInput = $('.confirm dd');
		$confirmInput.html('');
		$confirm.velocity('fadeOut', {
			duration: 300,
			easing: 'easeOutQuad',
			begin: function() {
				modalOff();
			}
		});
	});
	function modalOn(){
		$overlay.addClass('active');
		$all.addClass('no_scroll');
	}
	function modalOff(){
		$overlay.removeClass('active');
		$all.removeClass('no_scroll');
	}

	//フォーム送信 -----------------------//
	$doc.on('click','#send_btn',function(){
		$form.submit();
	});
	//form 二重送信禁止 ------------------//
	var $sendBtn = $('#send_btn');
	$form.on('submit', function () {
		$check.prop('disabled',false);
		$sendBtn.attr('disabled', 'disabled').val('送信中...');
	});
})(jQuery);
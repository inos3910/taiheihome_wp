(function($){
	"use strict";
	var $win = $(window);
	var $doc = $(document);
	var $this;
	//スマホタッチイベント制御 ------------//
	function spTouch(func){
		if ('touchstart' === event.type){
			$this.attr('data-touchstarted', '');
			return;
		}
		if ('touchmove' === event.type){
			$this.removeAttr('data-touchstarted');
			return;
		}
		if ('undefined' !== typeof $this.attr('data-touchstarted')){
			func.call($this, event);
			$this.removeAttr('data-touchstarted');
		}
	}
	//アーカイブアコーディオン
	var tapItem = '.post_archive_year';
	$doc.on('touchstart touchmove touchend', tapItem, function(){
		$this = $(this);
		spTouch(archiveTapped);
	});
	$doc.on('click', tapItem, function(){
		$this = $(this);
		archiveTapped.call($this, event);
	});
	function archiveTapped(event){
		event.preventDefault();
		event.stopPropagation();
		var $target = $this.next('.post_archive_list');
		$target.slideToggle();
		$this.toggleClass('active');
	}
})(jQuery);
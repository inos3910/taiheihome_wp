(function($){
	"use strict";
	//変数 --------------------------//
	var $win = $(window);
	var w    = $win.innerWidth();
	var $doc = $(document);
	var $this;
	//スマホタッチイベント制御 ------------//
	function spTouch(func){
		if ('touchstart' === event.type){
			$this.attr('data-touchstarted', '');
			return;
		}
		if ('touchmove' === event.type){
			$this.removeAttr('data-touchstarted');
			return;
		}
		if ('undefined' !== typeof $this.attr('data-touchstarted')){
			func.call($this, event);
			$this.removeAttr('data-touchstarted');
		}
	}
	//アコーディオン ------------------//
	var tapItem = '.profile_item_text dt';
	if(w < 768) {
		$doc.on('touchstart touchmove touchend', tapItem, function(){
			$this = $(this);
			spTouch(panelTapped);
		});
	}
	function panelTapped(event){
		event.preventDefault();
		$this.toggleClass('active')
		$this.next('dd').slideToggle();
	}
})(jQuery);
(function($){
	"use strict";
	//変数 ---------------------------//
	var $win    = $(window);
	var $doc    = $(document);
	var $all    = $('html');
	var $body   = $('body');
	var confirm = '<div class="confirm"><dl><dt class="label">ご希望の展示場</dt><dd class="confirm_exhibition"></dd><dt class="label">ご希望の日時</dt><dd class="confirm_schedule"></dd><dt class="label">お名前</dt><dd class="confirm_name"></dd><dt class="label">ふりがな</dt><dd class="confirm_kana"></dd><dt class="label">メールアドレス</dt><dd class="confirm_email"></dd><dt class="label">TEL</dt><dd class="confirm_tel"></dd></dl><div class="btns_wrap"><div class="btn"><input type="submit" id="send_btn" value="この内容で送信する"></div><button class="btn back">戻る</button></div></div>';
	$body.append(confirm);

	//予約日を自動で本日に設定 ----------//
	var $reserveMonth = $('.reserve_month');
	var $reserveDate  = $('.reserve_date');

	//今日の日付データを変数todayに格納
	var today = new Date();

	//月・日を取得する
	var month = today.getMonth()+1;
	var day   = today.getDate();
	$win.on('load',function(){
		$reserveMonth.val(month);
		$reserveDate.val(day+1);
	});

	//validation --------------------//
	var $form         = $('#reserve_form');
	var $reserveMonth = $('#reserve_month');
	var $reserveDate  = $('#reserve_date');
	var $reserveTime  = $('#reserve_time');
	var $userName     = $('#user_name');
	var $userNameKana = $('#user_name_kana');
	var $userEmail    = $('#user_email');
	var $userTel      = $('#user_tel');
	$reserveDate.addClass('validate[required,maxSize[50]]');
	$userName.addClass('validate[required,maxSize[30]]');
	$userNameKana.addClass('validate[required,custom[kana],maxSize[30]]');
	$userEmail.addClass('validate[required,custom[email],maxSize[90]]');
	$userTel.addClass('validate[required,custom[phone],maxSize[15]]');
	$form.validationEngine('attach', {
		promptPosition:"inline",
	});

	//確認画面表示 -----------------------//
	var $confirm = $('.confirm');
	var $overlay = $('.overlay');
	var $exhibition     = $('#exhibition_select');
	var $confExhibition = $('.confirm_exhibition');
	var $confSchedule   = $('.confirm_schedule');
	var $confName       = $('.confirm_name');
	var $confKana       = $('.confirm_kana');
	var $confEmail      = $('.confirm_email');
	var $confTel        = $('.confirm_tel');
	$doc.on('click','#confirm_btn',function(e){
		e.preventDefault();
		if($form.validationEngine('validate')===true){
			$confExhibition.text($exhibition.val());
			$confSchedule.text($reserveMonth.val()+'月'+$reserveDate.val()+'日'+$reserveTime.val()+'時');
			$confName.text($userName.val());
			$confKana.text($userNameKana.val());
			$confEmail.text($userEmail.val());
			$confTel.text($userTel.val());
			$confirm.velocity('fadeIn', {
				duration: 300,
				easing: 'easeOutQuad',
				begin: function() {
					modalOn();
				}
			});
		}
	});

	//戻るボタン ------------------------//
	$doc.on('click','.back',function(e){
		e.preventDefault();
		var $confirmInput = $('.confirm dd');
		$confirmInput.html('');
		$confirm.velocity('fadeOut', {
			duration: 300,
			easing: 'easeOutQuad',
			begin: function() {
				modalOff();
			}
		});
	});
	function modalOn(){
		$overlay.addClass('active');
		$all.addClass('no_scroll');
	}
	function modalOff(){
		$overlay.removeClass('active');
		$all.removeClass('no_scroll');
	}
	
	//フォーム送信 -----------------------//
	$doc.on('click','#send_btn',function(){
		$form.submit();
	});
	//form 二重送信禁止 ------------------//
	var $sendBtn = $('#send_btn');
	$form.on('submit', function () {
		$sendBtn.attr('disabled', 'disabled').val('送信中...');
	});
})(jQuery);
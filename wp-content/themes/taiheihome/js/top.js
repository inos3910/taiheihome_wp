(function($){
	"use strict";
	//swiper ---------------------//
	var swiper = new Swiper('.swiper-container', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		slidesPerView: 1,
		spaceBetween: 0,
		loop: true,
		autoplay: 3000,
		autoplayDisableOnInteraction: false,
		parallax: true,
		speed: 600,
		touchRatio: 2,
		keyboardControl: true
	});
})(jQuery);
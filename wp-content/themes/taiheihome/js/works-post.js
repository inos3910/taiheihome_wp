(function($){
	"use strict";
	//sliderPro --------------------------//
	var $carousel = $('#carousel');
	$carousel.sliderPro({
		width: 600,
		height: 400,
		fade: true,
		arrows: false,
		buttons: false,
		thumbnailWidth: 140,
		thumbnailHeight: 110,
		thumbnailArrows: true,
		autoplay: false,
		imageScaleMode: 'exact',
		breakpoints: {
			568: {
				thumbnailWidth: 100,
				thumbnailHeight: 78,
				thumbnailArrows: false,
				imageScaleMode: 'exact',
			}
		},
		init: function(){
			$carousel.velocity({
				opacity: 1,
			},
			{
				duration: 300,
				easing: 'ease-in-out',
			});
		}
	});
})(jQuery);
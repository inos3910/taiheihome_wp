(function($){
	"use strict";
	var $more   = $('#more');
	var $worksList = $('.works_list');
	var exclude = [];
	$more.on('click',function(){
		var $postItem = $worksList.find('li');
		$postItem.each(function (i,elem) {
			var ex = $(elem).data('ex');
			exclude.push(ex);
			// 配列の重複を削除
			exclude = exclude.filter(function (x, i, self) {
				return self.indexOf(x) === i;
			});
		});
		$.ajax({
			type: 'post',
			url: ajaxurl,
			cache: false,
			dataType: 'json',
			async: true,
			data: {
				'action': 'works_post',
				'exclude': exclude
			}
		}).done(function( data, textStatus ) {
			$worksList.append(data);
			$postItem = $worksList.find('li');
			$postItem.each(function (i,elem) {
				var ex = $(elem).data('ex');
				exclude.push(ex);
			  // 配列の重複を削除
			  exclude = exclude.filter(function (x, i, self) {
			  	return self.indexOf(x) === i;
			  });
			  if($(elem).hasClass('add')){
			  	$(elem).delay(400).queue(function() {
			  		$(elem).addClass('show').dequeue();
			  	});
			  }
			});
			var totalPosts = $postItem.length;
			var maxPosts   = $postItem.first().data('all');
			if( totalPosts === maxPosts ){
				$more.addClass('remove')
			}
		})
		.fail(function( jqXHR, textStatus ) {
			console.log(textStatus);
		});
	});
})(jQuery);
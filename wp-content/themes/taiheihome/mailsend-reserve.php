<?php
/**
* Template Name: 来場予約送信
* ------------------------//
* fileName : mailsend-reserve.php
* content : 来場予約フォーム送信
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
session_start();

//エンコード
mb_language("ja");
mb_internal_encoding("UTF-8");

//セキュリティ対策class
class common{
	public static function getGET($name){
		$ret = filter_input(INPUT_GET, $name);
		if (isset($ret)){
            $ret = str_replace("\0", "", $ret);//Nullバイト攻撃対策
            return htmlspecialchars($ret, ENT_QUOTES, 'UTF-8');
        }
        return '';
    }

    public static function getPost($name){
    	$ret = filter_input(INPUT_POST, $name);
    	if (isset($ret)){
            $ret = str_replace("\0", "", $ret);//Nullバイト攻撃対策
            return htmlspecialchars($ret, ENT_QUOTES, 'UTF-8');
        }
        return '';
    }

    public static function getCookie($name){
    	$ret = filter_input(INPUT_COOKIE, $name);
    	if (isset($ret)){
            $ret = str_replace("\0", "", $ret);//Nullバイト攻撃対策
            return htmlspecialchars($ret, ENT_QUOTES, 'UTF-8');
        }
        return '';
    }
}

/*------------------------------------------------------------------------*/

//入力項目のサニタイズ・エスケープ
$exhibition_select = common::getPost('exhibition_select');
$reserve_month     = common::getPost('reserve_month');
$reserve_date      = common::getPost('reserve_date');
$reserve_time      = common::getPost('reserve_time');
$user_name         = common::getPost('user_name');
$user_name_kana    = common::getPost('user_name_kana');
$user_email        = common::getPost('user_email');
$user_tel          = common::getPost('user_tel');

/*-----------------------------------------------------------------------*/

/* サイト管理者メールアドレス取得 */
$admin_info  = get_userdata(1);
$admin_email = $admin_info->user_email;
//エスケープ処理
$admin_email = str_replace("\0", "", $admin_email);//Nullバイト攻撃対策
$admin_email = htmlspecialchars($admin_email, ENT_QUOTES, 'UTF-8');

/* 送信者MailAdress*/
$to = $admin_email;

/* 送信側の件名 */
$SUBJ_1 = "【太平ホーム】展示場見学のご予約誠にありがとうございます。";

/* 通知側の件名 */
$SUBJ_2 = "【展示場来場ご予約】".$user_name."様";

/* 送信者情報(メールヘッダー) */
$headers = array (
    'Content-Type: text/plain; charset=ISO-2022-JP',
    'From: 太平ホーム <'.$admin_email.'>',
    'Reply-To' => "<".$admin_email.">"
    );

/*--------------------------------------------------------------------------------------*/
//自動返信内容（ユーザー）
$auto_reply ="";//初期化
$auto_reply .="\n";
$auto_reply .=$user_name."様\n";
$auto_reply .="\n";
$auto_reply .="この度は展示場のご見学をご予約いただき、誠にありがとうございます。\n";
$auto_reply .="担当者から折り返しご連絡致しますので今しばらくお待ちください。\n";
$auto_reply .="\n";
$auto_reply .="========= お客様のご入力された内容 =========\n";
$auto_reply .="\n";
$auto_reply .="【ご希望の展示場】\n";
$auto_reply .=$exhibition_select."\n";
$auto_reply .="\n";
$auto_reply .="【ご希望の日時】\n";
$auto_reply .=$reserve_month."月".$reserve_date."日".$reserve_time."時"."\n";
$auto_reply .="\n";
$auto_reply .="【お名前】\n";
$auto_reply .=$user_name."\n";
$auto_reply .="\n";
$auto_reply .="【ふりがな】\n";
$auto_reply .=$user_name_kana."\n";
$auto_reply .="\n";
$auto_reply .="【メールアドレス】\n";
$auto_reply .=$user_email."\n";
$auto_reply .="\n";
$auto_reply .="【TEL】\n";
$auto_reply .=$user_tel."\n";
$auto_reply .="\n";

/*--------------------------------------------------------------------------------------*/
//受付側内容（会社）
$mail_message ="";//初期化
$mail_message .="\n";
$mail_message .="展示場の来場予約が入りました。\n";
$mail_message .="詳細は次の通りとなっております。\n";
$mail_message .="ご確認・ご返答の程宜しくお願いいたします。\n";
$mail_message .="\n";
$mail_message .="========= お客様のご入力された内容 =========\n";
$mail_message .="\n";
$mail_message .="【ご希望の展示場】\n";
$mail_message .=$exhibition_select."\n";
$mail_message .="\n";
$mail_message .="【ご希望の日時】\n";
$mail_message .=$reserve_month."月".$reserve_date."日".$reserve_time."時"."\n";
$mail_message .="\n";
$mail_message .="【お名前】\n";
$mail_message .=$user_name."\n";
$mail_message .="\n";
$mail_message .="【ふりがな】\n";
$mail_message .=$user_name_kana."\n";
$mail_message .="\n";
$mail_message .="【メールアドレス】\n";
$mail_message .=$user_email."\n";
$mail_message .="\n";
$mail_message .="【TEL】\n";
$mail_message .=$user_tel."\n";
$mail_message .="\n";

/*--------------------------------------------------------------------------------------*/


if( wp_mail( $user_email, $SUBJ_1, $auto_reply, $headers) && wp_mail( $to, $SUBJ_2, $mail_message, $headers) ){
	//セッション変数をクリア
	$_SESSION = array();
    session_destroy();
    header("Location: {$_SERVER['HTTP_REFERER']}/complete/");
    exit;
} else {
	//送信エラーの場合に再入力する項目
	$_SESSION['s_user_name']      = $user_name;
	$_SESSION['s_user_name_kana'] = $user_name_kana;
	$_SESSION['s_user_email']     = $user_email;
	$_SESSION['s_user_tel']       = $user_tel;
	$_SESSION['error']            = "メールの送信に失敗しました。恐れ入りますが、お電話でお問い合わせ頂くか、もう一度最初からご入力ください。";
	header("Location: {$_SERVER['HTTP_REFERER']}");
	exit;
}
?>
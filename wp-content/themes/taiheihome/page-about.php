<?php
/**
* ------------------------//
* fileName : page-about.php
* content : 私たちについてページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="about_contents">
    <div class="about_title_wrap page_title_wrap">
      <h1 class="about_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-about.png" height="48" width="234" alt="ABOUT"><span>私たちについて</span></h1>
      <div class="about_page_down">
        <a href="#container"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/page-bottom.png" height="84" width="84" alt="下へ"></a>
      </div>
      <!-- /.about_page_down -->
    </div>
    <!-- /.page_title_wrap -->
    <div class="about_feature about_parallax">
      <div class="about_feature_inner">
        <h2 class="about_feature_band">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/feature-copy.png" height="735" width="177" alt="つながる日々へ、還ろう。">
        </h2>
        <!-- /.about_feature_band -->
      </div>
      <!-- /.about_feature_inner -->
      <div class="about_feature_text">
        <p>
          <span>家族と、つながる。</span>
          <span>仲間と、つながる。</span>
          <span>地域と、つながる。</span>
          <span>そんな日々の積み重ねが、</span>
          <span> かけがえのない人生を織り成していく。</span>
          <span>地域に根ざした工務店の仕事は、</span>
          <span>そのための舞台をつくることでした。</span>
          <span>どれだけ時代が変化しても、</span>
          <span>太平ホームはその原点を忘れません。</span>
          <span>さあ、いっしょに、</span>
          <span>つながる日々へと還りましょう。</span>
        </p>
      </div>
      <!-- /.about_feature_text -->
    </div>
    <!-- /.about_feature -->
    <div class="about_parallax_wrap" id="container">
      <div class="about_parallax about_parallax_01">
        <div class="about_contents_inner">
          <div class="about_parallax_box">
            <h2 class="about_parallax_caption">暮らしの歓びを<span>広げるプランニング</span></h2>
            <p class="about_parallax_text_01">
              <span>太平ホームが目指すのは、</span>
              <span>毎日、帰宅するのが待ち遠しくなる住まい。</span>
              <span>ふれあいを豊かにする工夫をたっぷりと詰め込み、</span>
              <span>家族みんなでくつろぐ時間も、</span>
              <span>ひとりで趣味に向き合う時間も、</span>
              <span>もっと楽しくなる空間をご提供します。</span>
            </p>
            <!-- /.about_parallax_text_01 -->
          </div>
          <!-- /.about_parallax_box -->
        </div>
        <!-- /.about_contents_inner -->
      </div>
      <!-- /.about_parallax_01 -->
      <div class="about_parallax about_parallax_02">
        <div class="about_contents_inner">
          <div class="about_parallax_box">
            <h2 class="about_parallax_caption">心地よさが末永く<span>続いていくクオリティ</span></h2>
            <p class="about_parallax_text_02">
              <span>豊富な経験とノウハウを活かしつつ、</span>
              <span>最新の技術や知識も採り入れ、</span>
              <span>家族のしあわせな時間が末永くつづいていく</span>
              <span>安心の住まいを築きます。</span>
              <span>省エネ・断熱性にもこだわり、地球と家計にやさしく、</span>
              <span>次代に誇れる住宅品質を追求します。</span>
            </p>
            <!-- /.about_parallax_text_02 -->
          </div>
          <!-- /.about_parallax_box -->
        </div>
        <!-- /.about_contents_inner -->
      </div>
      <!-- /.about_parallax_02 -->
      <div class="about_parallax about_parallax_03">
        <div class="about_contents_inner">
          <div class="about_parallax_box">
            <h2 class="about_parallax_caption">「地力」を活かした<span>アフターサービス</span></h2>
            <p class="about_parallax_text_03">
              <span>杉戸町を中心とした地元密着企業として、</span>
              <span>迅速かつ丁寧なアフターサービスをお約束します。</span>
              <span>どんな小さなことでも真摯に相談に乗り、</span>
              <span>みなさまの暮らしをすぐ近くで見守り続ける、</span>
              <span>昔ながらの工務店であり続けます。</span>
            </p>
            <!-- /.about_parallax_text_03 -->
          </div>
          <!-- /.about_parallax_box -->
        </div>
        <!-- /.about_contents_inner -->
      </div>
      <!-- /.about_parallax_03 -->
    </div>
    <!-- /.about_parallax_wrap -->
  </div>
  <!-- /.about_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

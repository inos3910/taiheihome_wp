<?php
/**
* ------------------------//
* fileName : page-after.php
* content : アフターメンテナンスページ
* last updated : 20160419
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="after_contents">
    <div class="after_title_wrap page_title_wrap">
      <h1 class="after_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-after.png" height="49" width="688" alt="AFTER MAINTENANCE"><span>アフターメンテナンス</span></h1>
    </div>
    <!-- /.after_title_wrap.page_title_wrap -->
    <div class="after_contents_inner">
      <div class="after_top">
        <h2 class="caption">いつまでも<br>永く安心して<br>お住まいください。</h2>
        <p>お客様が高品質で安全な住宅に安心して暮らしていただけるよう、<br>しっかりとした構造躯体と精度の高い施工技術、きめ細かな検査体制による家づくりはもちろんのこと、<br>長期にわたってお住まいの品質・性能を維持できるよう、<br>お引渡し後には定期的な点検とメンテナンス工事を実施しております。</p>
      </div>
      <!-- /.after_top -->
      <div class="after_middle">
        <div class="after_middle_inner after_middle_inner_01">
          <div class="text">
            <h3 class="caption">注文建築の<br>30年保証システム</h3>
            <p>構造躯体および防水については、<br>当初 10 年の保証期間満了時と、<br>延長した保証期間の満了時に行う<br>定期点検とメンテナンス工事により、<br>最大２回の延長によって<br>最長 30 年間の保証期間と致します。</p>
          </div>
          <!-- /.text -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/after-pic-01.jpg" height="731" width="933" alt="定期点検時期および保証延長のイメージ図">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.after_middle_inner_01 -->
        <h3 class="caption">24時間365日受付の<br>アフターサポートセンター<br>を設置</h3>
        <div class="after_middle_inner after_middle_inner_02">
          <p class="text">お施主様からの住まいに関するご相談を、24時間365日体制で<br>承る電話対応サービスです。電話による状況確認から修理の手配まで<br>専門スタッフが責任を持って対応します。<br>またアフターサポートセンターをご利用いただいた際の修理履歴は<br>お施主様個別の住宅履歴情報管理サイトに登録され、<br>施主様ご自身がパソコンから閲覧することも可能です。<br>
          </p>
          <!-- /.text -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/after-pic-02.jpg" height="277" width="433" alt="オペレーターの女性">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.after_middle_inner_02 -->
        <p class="note">※当ページに記載されている保証システムについては平成25年4月以降にお引き渡ししたオーナー様向けです。<br>※アフターサポートセンターについては平成23年1月以降にお引き渡ししたオーナー様向けの内容です。</p>
      </div>
      <!-- /.after_middle -->
      <div class="after_bottom">
        <div class="after_link_wrap">
          <div class="after_link_item">
            <p>住宅履歴管理サービスWEBサイトはこちら</p>
            <a href="https://www.karute.jp/Owner/login.aspx" target="_blank" class="btn">住宅履歴管理サービス</a>
          </div>
          <!-- /.after_link_item -->
          <div class="after_link_item">
            <p>家のお手入れの方法についてのアドバイス</p>
            <a href="http://www.taiheihome.co.jp/aftermaintenance/#maintenance-center" target="_blank" class="btn">メンテナンスセンター</a>
          </div>
          <!-- /.after_link_item -->
        </div>
        <h4 class="caption">点検・メンテナンスに関するご相談は</h4>
        <div class="after_link_wrap">
          <div class="after_link_item">
            <a href="<?php echo home_url('/');?>contact/" class="btn">お問い合わせフォーム</a>
          </div>
          <!-- /.after_link_item -->
          <div class="after_link_item">
            <a href="tel:0120-59-1601" class="tel"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tel-after-gray.png" height="79" width="733" alt="0120-59-1601"></a>
            <span>（OPEN: 10:00〜18:00）</span>
            <span>定休日:第1・3・5火曜日、毎週水曜日</span>
          </div>
          <!-- /.after_link_item -->
        </div>
        <!-- /.after_link_wrap -->
      </div>
      <!-- /.after_bottom -->
    </div>
    <!-- /.after_contents_inner -->
  </div>
  <!-- /.after_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

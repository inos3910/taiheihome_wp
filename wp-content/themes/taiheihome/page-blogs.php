<?php
/**
* ------------------------//
* fileName : page-blogs.php
* content : ブログ一覧ページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="news_contents">
    <div class="news_title_wrap page_title_wrap">
      <h1 class="news_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-blog.png" height="48" width="175"alt="BLOG"><span>ブログ一覧</span></h1>
    </div>
    <!-- /.page_title_wrap -->
    <div class="news_contents_inner">
      <ul class="news_list">
        <?php
        global $blog_query;
        $blog_args = array( 
          'post_type' => 'post',
          'posts_per_page' => 5,
          'order' => 'DESC',
          'orderby' => 'date',
          'paged'=>$paged
          );
        $blog_query = new WP_Query( $blog_args );
        if ( $blog_query->have_posts() ) :
          while ( $blog_query->have_posts() ) : $blog_query->the_post();
          //ループ開始****************************************************
        ?>
        <li class="news_item">
          <div class="news_item_date"><?php the_time('Y.m.d'); ?></div>
          <div class="news_item_content">
            <a href="<?php the_permalink(); ?>">
              <h2 class="news_item_title"><?php the_title();?></h2>
              <p><?php the_excerpt(); ?></p>
            </a>
          </div>
          <!-- /.news_item_content -->
        </li>
        <!-- /.news_item -->
        <?php //ループ終了**************************************************************************
        endwhile;
        endif;
        wp_reset_postdata();
        ?>
      </ul>
      <!-- /.news_list -->
      <?php if(function_exists('wp_pagenavi')) wp_pagenavi(array('query' => $blog_query)); ?>
    </div>
    <!-- /.news_contents_inner -->
  </div>
  <!-- /.news_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>
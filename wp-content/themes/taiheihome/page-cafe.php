<?php
/**
* ------------------------//
* fileName : page-cafe.php
* content : T's ZERO+カフェページ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_detail_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.product_title_wrap.page_title_wrap -->
    <div class="product_detail_contents_inner">
      <div class="product_detail_visual cafe_visual">
        <h2 class="product_detail_logo">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cafe-l.png" height="322" width="418" alt="T's ZERO+カフェ">
        </h2>
        <!-- /.product_detail_visual.cafe_visual -->
      </div>
      <!-- /.product_detail_visual -->
      <div class="cafe_concept">
        <div class="cafe_concept_inner">
          <div class="cafe_concept_contents">
            <h2 class="product_concept_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept.png" height="38" width="183" alt="concept"></span>空気感にこだわる。</h2>
            <p class="product_concept_text">クラシカのコンセプトに<br>「カフェスタイル」の要素を採り入れた注文建築。<br>まるでお気に入りのカフェで過ごすような<br>心地よさがずっと続いていきます。</p>
            <div class="cafe_concept_pic_01">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-concept-01.jpg" height="554" width="1060" alt="キッチンスペース">
            </div>
            <!-- /.cafe_concept_pic_01 -->
          </div>
          <!-- /.cafe_concept_contents -->
          <div class="cafe_concept_pic_02">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-concept-02.jpg" height="1067" width="775" alt="カフェ 外観">
          </div>
          <!-- /.cafe_concept_pic_02 -->
        </div>
        <!-- /.cafe_concept_inner -->
      </div>
      <!-- /.cafe_concept -->
      <div class="product_concept_plan">
        <h2 class="product_concept_plan_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept-plan.png" height="44" width="298" alt="concept plan."></span>内と外を緩やかにつなぎ、<br>光と風をたっぷりと。</h2>
        <div class="product_concept_plan_box">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">暮らしを便利につなぐ<br>回遊家事動線</h3>
              <p>キッチン〜廊下〜洗面室〜ミセスコーナーとぐるりと回遊できる動線を設計。毎日の家事の効率がぐっと向上するうれしいプランです。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">爽やかな光と風が<br>広がりを演出するLDK</h3>
              <p>開口部から届く光が、空間にさらなる広がりを演出。くつろぎの空気がゆっくりと満ちていくLDKです。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">衣類をすっきりと<br>収納できるW.I.C</h3>
              <p>主寝室にはW.I.C.を２つも設計。奥様とご主人、それぞれの衣類を分けてすっきりと収納できます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor cafe_guide gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-floor-all.jpg" data-size="830x805">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-floor.jpg" height="1013" width="1734" alt="カフェ 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">内と外をつなぐ<br>インナーテラス</h3>
              <p>東西のウッドデッキをつなぐように、吹抜けのあるインナーテラスを設計。陽だまりの中で憩える開放感あふれる空間です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">暮らしの可能性を広げる<br>Ｗウッドデッキ</h3>
              <p>LDK側だけでなく、水まわりスペース側にもウッドデッキを設計。洗濯物を干すスペースなどとして重宝していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">親子で並んで読書できる<br>ファミリーラボ</h3>
              <p>ご夫婦の書斎として、お子様の勉強スペースとして。デスク＆ラックを造作した、ご家族共用のライブラリースペースです。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box -->
      </div>
      <!-- /.product_concept_plan -->
      <div class="product_inner_space">
        <h2 class="product_inner_space_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-inner-space.png" height="40" width="271" alt="inner space"></span>豊かな風合いが、<br>手ざわりのいい<br>時間をつくる。</h2>
        <div class="product_inner_space_grid product_inner_space_grid_4 gallery_group">
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-01.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-01.jpg" height="385" width="421" alt="キッチン">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-02.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-02.jpg" height="385" width="421" alt="ダイニング">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-03.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-03.jpg" height="385" width="421" alt="照明">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-04.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-04.jpg" height="385" width="421" alt="キッチン">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-05.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-05.jpg" height="385" width="421" alt="洗面台">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-06.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-06.jpg" height="385" width="421" alt="食卓">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-07.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-07.jpg" height="385" width="421" alt="フリースペース">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-08.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-inner-space-08.jpg" height="385" width="421" alt="カウンターキッチン">
            </a>
          </figure>
        </div>
        <!-- /.product_inner_space_grid.product_inner_space_grid_4 -->
      </div>
      <!-- /.product_inner_space -->
      <div class="product_facilities">
        <h2 class="product_facilities_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-facilities.png" height="46" width="208" alt="facilities."></span>機能性だけでなく、<br>美しさも追求した<br>先進設備。</h2>
        <div class="product_facilities_inner">
          <div class="product_facilities_contents">
            <h3 class="caption">何でもない時間も<br>絵になるように。</h3>
            <p>「T’s ZERO ＋カフェ」の設備には、機能性と意匠性のどちらも追求した厳選アイテムを採用しています。デザインはもちろん素材にもこだわった高感度なアイテムたちが、空間の心地よさをさらに高めます。</p>
          </div>
          <!-- /.product_facilities_contents -->
          <div class="product_facilities_pic_01">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-facilities-01.jpg" height="496" width="908" alt="素材にこだわった家具・木目のキッチン">
          </div>
        </div>
        <!-- /.product_facilities_inner -->
        <ul class="product_facilities_list">
          <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-facilities-02.jpg" height="285" width="423" alt="床">
          </li>
          <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-facilities-03.jpg" height="285" width="423" alt="天井">
          </li>
          <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-facilities-04.jpg" height="285" width="423" alt="壁">
          </li>
          <li>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cafe-facilities-05.jpg" height="285" width="423" alt="扉">
          </li>
        </ul>
        <!-- /.product_facilities_list -->
        <?php get_template_part('block','btns'); ?>
      </div>
      <!-- /.product_facilities -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_list.product_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.product_detail_contents_inner -->
  </div>
  <!-- /.product_detail_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

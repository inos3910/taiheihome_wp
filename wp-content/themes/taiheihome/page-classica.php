<?php
/**
* ------------------------//
* fileName : page-classica.php
* content : T's ZEROクラシカページ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_detail_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.product_title_wrap.page_title_wrap -->
    <div class="product_detail_contents_inner">
      <div class="product_detail_visual classica_visual">
        <h2 class="product_detail_logo">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-classica-l.png" height="302" width="415" alt="T's ZERO クラシカ">
        </h2>
        <!-- /.product_detail_logo -->
      </div>
      <!-- /.product_detail_visual -->
      <div class="classica_concept">
        <div class="classica_concept_inner">
          <div class="classica_concept_contents">
            <h2 class="product_concept_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept.png" height="38" width="183" alt="concept"></span>家づくりの原点へ。</h2>
            <p class="product_concept_text">クラシカは、「くらし＋クラシック＋家」を組み合わせた言葉です。<br>家づくりの原点（classic）に立ち返り、<br>流行に左右されない普遍的な心地よさを追求した注文建築。<br>この住まいが、つながる日々のスタート地点になります。</p>
          </div>
          <!-- /.classica_concept_contents -->
          <div class="classica_concept_pic_01">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-concept-01.jpg" height="464" width="804" alt="クラシカ 外観">
          </div>
          <!-- /.classica_concept_pic_01 -->
          <div class="classica_concept_pic_02">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-concept-02.jpg" height="494" width="1181" alt="クラシカ 内観 階段">
          </div>
          <!-- /.classica_concept_pic_02 -->
          <div class="classica_concept_pic_03">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-concept-03.jpg" height="494" width="657" alt="クラシカ リビング 和室">
          </div>
          <!-- /.classica_concept_pic_03 -->
        </div>
        <!-- /.classica_concept_inner -->
      </div>
      <!-- /.classica_concept -->
      <div class="product_concept_plan">
        <h2 class="product_concept_plan_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept-plan.png" height="44" width="298" alt="concept plan."></span>ご家族のつながりを<br>豊かにする工夫を、<br>すみずみに。</h2>
        <div class="product_concept_plan_box">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">おいしい会話をつくり出す<br>対面カウンターキッチン</h3>
              <p>お料理をしているときも、ご家族との会話を楽しめるように。キッチンは対面カウンタータイプを採用しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいをはぐくむ<br>リビング階段</h3>
              <p>キッチンのすぐとなりに２階へとつながる階段をレイアウト。おでかけ時も、帰宅時も、親子の会話を自然とはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">衣類をすっきりと<br>収納できるW.I.C</h3>
              <p>主寝室には3帖のゆとりあるウォークインクローゼットを設計。ご夫婦の衣類をまとめて収納できます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor clasica_guide gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-floor-all.jpg" data-size="830x890">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-floor.jpg" height="1208" width="1727" alt="クラシカ 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">和やかな団らんを<br>演出する広々LDK</h3>
              <p>LDKには19帖のゆとりを確保。爽やかな光と風が、ご家族団らんの時をのびやかに演出します。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">LDKと一体利用もできる<br>情趣豊かなモダン和室</h3>
              <p>小さなお子様のお昼寝スペースとしても重宝する和の空間。LDKと一体利用すれば、25帖超の団らん空間に。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">風を感じながら憩える<br>ウッドデッキ</h3>
              <p>ウッドデッキは、空の下でゆったりと憩えるもうひとつのダイニング。BBQパーティなどにも活用できます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box -->
      </div>
      <!-- /.product_concept_plan -->
      <div class="product_inner_space">
        <h2 class="product_inner_space_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-inner-space.png" height="40" width="271" alt="inner space"></span>さりげないこだわりが、<br>暮らしの歓びを広げる。</h2>
        <div class="product_inner_space_grid gallery_group">
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-01.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-01.jpg" height="385" width="585" alt="子供の勉強スペース">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-02.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-02.jpg" height="385" width="585" alt="寝室">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-03.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-03.jpg" height="385" width="585" alt="リビング 階段">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-04.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-04.jpg" height="385" width="585" alt="階段">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-05.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-05.jpg" height="385" width="585" alt="子供部屋">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-06.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/classica-inner-space-06.jpg" height="385" width="585" alt="風呂 洗面所">
            </a>
          </figure>
        </div>
        <!-- /.product_inner_space_grid -->
        <?php get_template_part('block','btns'); ?>
      </div>
      <!-- /.product_inner_space -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_list.product_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.product_detail_contents_inner -->
  </div>
  <!-- /.product_detail_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

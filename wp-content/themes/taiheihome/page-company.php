<?php
/**
* ------------------------//
* fileName : page-company.php
* content : 会社概要ページ
* last updated : 20160419
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="company_contents">
    <div class="company_title_wrap page_title_wrap">
      <h1 class="profile_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-profile.png" height="48" width="294" alt="PROFILE"><span>会社概要</span></h1>
    </div>
    <!-- /.conpany_title_wrap.page_title_wrap -->
    <div class="company_contents_inner">
      <div class="company_detail">
        <div class="company_detail_table">
          <dl>
            <dt>会社名</dt>
            <dd>太平ホーム株式会社</dd>
          </dl>
          <dl>
            <dt>設立</dt>
            <dd>1978年5月</dd>
          </dl>
          <dl>
            <dt>創立</dt>
            <dd>1978年5月</dd>
          </dl>
          <dl>
            <dt>資本金</dt>
            <dd>100,000千円</dd>
          </dl>
          <dl>
            <dt>代表取締役社長</dt>
            <dd>荒島 敏彦</dd>
          </dl>
          <dl>
            <dt>本社所在地</dt>
            <dd>埼玉県北葛飾郡杉戸町杉戸2丁目7番3号</dd>
          </dl>
          <dl>
            <dt>事業内容</dt>
            <dd>
              <span>注文建築の設計・施工、分譲住宅の設計施工・販売</span>
              <span>不動産の仲介・販売、リフォーム事業、損害保険代理店業務</span>
            </dd>
          </dl>
          <dl>
            <dt>登録免許</dt>
            <dd>
              <span>宅地建物取引業許可 埼玉県知事（10）第8089号</span>
              <span>建設業許可 埼玉県知事(特・24)第36615号</span>
              <span>一級建築士事務所 埼玉県知事(6)第1889号</span>
            </dd>
          </dl>
          <dl>
            <dt>加入団体名</dt>
            <dd>
              <span>（公社）埼玉県宅地建物取引業協会</span>
              <span>（公社）全国宅地建物取引業保証協会</span>
              <span>（公社）全国住宅産業協会</span>
              <span>（公社）首都圏不動産公正取引協議会加盟</span>
            </dd>
          </dl>
        </div>
        <!-- /.company_detail_table -->
      </div>
      <!-- /.company_detail -->
      <div class="company_history">
        <div class="company_history_inner">
          <h2 class="company_history_caption">会社沿革</h2>
          <dl>
            <dt>1978年（昭和53年）</dt>
            <dd>株式会社首都圏住宅流通センター設立</dd>
            <dt>1985年（昭和60年）</dt>
            <dd>太平ホーム株式会社に社名変更</dd>
            <dt>2001年（平成13年）</dt>
            <dd>杉戸住宅展示場 "Project1000" 開設</dd>
            <dt>2005年（平成17年）</dt>
            <dd>久喜住宅展示場 "やわら木" 開設</dd>
            <dt>2006年（平成18年）</dt>
            <dd>杉戸住宅展示場 "f" 開設</dd>
            <dt>2008年（平成20年）</dt>
            <dd>久喜青毛住宅展示場 "Ai"  開設</dd>
            <dt>2011年（平成23年）</dt>
            <dd>杉戸住宅展示場 "Project1000" の建替えにより "S" 開設</dd>
            <dt>2012年（平成24年）</dt>
            <dd>"ハウジングセンター" 開設</dd>
            <dt>2015年（平成24年）</dt>
            <dd>本社住宅展示場開設</dd>
            <dt>2015年（平成27年）</dt>
            <dd>エコステージ東武動物公園が杉戸町初 "埼玉県子育て応援住宅認定" 取得</dd>
            <dt>2015年（平成27年）</dt>
            <dd>"メンテナンスセンター" 開設</dd>
            <dt>2016年（平成28年）</dt>
            <dd>新商品としてT’s ZEROシリーズとT’s  ONEシリーズを発表</dd>
          </dl>
        </div>
        <!-- /.company_history_inner -->
      </div>
      <!-- /.company_history -->
      <div class="company_aside">
        <div class="company_oneside">
          <div class="company_office">
            <h2 class="company_aside_caption">OFFICE<span>本社</span></h2>
            <div class="company_oneside_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-office.jpg" height="292" width="543" alt="太平ホーム本社">
              <h3 class="name">太平ホーム本社</h3>
              <p class="address">埼玉県北葛飾郡杉戸町杉戸2-7-3</p>
              <div class="tel">
                <a href="tel:0480-34-1601">TEL. 0480-34-1601</a>
                <span>FAX. 0480-32-8847</span>
              </div>
              <!-- /.tel -->
              <a href="https://goo.gl/maps/DSo85qyDbqq" target="_blank" class="btn">Googleマップで見る</a>
            </div>
            <!-- /.company_oneside_item -->
          </div>
          <!-- /.company_office -->
          <div class="company_maintenance">
            <h2 class="company_aside_caption">MAINTENANCE<span>メンテナンス<br>センター</span></h2>
            <div class="company_oneside_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-maintenance.jpg" height="292" width="542" alt="メンテナンスセンター">
              <h3 class="name">メンテナンスセンター</h3>
              <p class="address">埼玉県北葛飾郡杉戸町内田2-11-4</p>
              <div class="tel">
                <a class="freedial" href="tel:0120-59-1601">0120-59-1601</a>
                <a href="tel:0480-32-1101">TEL.0480-32-1101</a>
              </div>
              <!-- /.tel -->
              <a href="https://goo.gl/maps/Lsp8KRDeUYN2" target="_blank" class="btn">Googleマップで見る</a>
            </div>
            <!-- /.company_oneside_item -->
          </div>
          <!-- /.company_maintenance -->
        </div>
        <!-- /.company_oneside -->
        <div class="company_model_house">
          <h2 class="company_aside_caption">MODEL HOUSE<span>展示場</span></h2>
          <div class="company_model_house_item_wrap">
            <div class="company_model_house_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-modelhouse-01.jpg" height="292" width="542" alt="太平ホーム ハウジングセンター">
              <h3 class="name">ハウジングセンター</h3>
              <p class="address">埼玉県北葛飾郡杉戸町内田2-5-7</p>
              <p class="note">数多くの土地や分譲住宅情報、また施工事例や資金計画を立てるための情報も満載。</p>
            </div>
            <!-- /.company_model_house_item -->
            <div class="company_model_house_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-modelhouse-02.jpg" height="292" width="543" alt="本社住宅展示場（ハウジングセンター併設）">
              <h3 class="name">本社住宅展示場<span class="row">（ハウジングセンター併設）</span></h3>
              <p class="address">埼玉県北葛飾郡杉戸町内田2-5-7</p>
              <p class="note">4つの展示場の中で最も大きく、ハイグレード仕様。おもてなし空間を随所に設計。</p>
            </div>
            <!-- /.company_model_house_item -->
            <a href="https://goo.gl/maps/BFzu3ocAYJz" target="_blank" class="btn">Googleマップで見る</a>
          </div>
          <!-- /.company_model_house_item_wrap -->
          <div class="company_model_house_item_wrap">
            <div class="company_model_house_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-modelhouse-03.jpg" height="329" width="542" alt="久喜青毛住宅展示場">
              <h3 class="name">久喜青毛住宅展示場<span>[T’s ZEROクラシカ]</span></h3>
              <p class="address">埼玉県久喜市青毛1-7-12</p>
              <p class="note">リビングに自然と家族が集まる工夫をほどこした、子育て応援プランをご提案。</p>
              <a href="https://goo.gl/maps/HK5vC8c8p5y" target="_blank" class="btn">Googleマップで見る</a>
            </div>
            <!-- /.company_model_house_item -->
            <div class="company_model_house_item">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/company-modelhouse-04.jpg" height="329" width="547" alt="杉戸住宅展示場">
              <h3 class="name">杉戸住宅展示場<span>[T’s ZEROフィット][T’s ZEROクラシカ]</span></h3>
              <p class="address">埼玉県北葛飾郡杉戸町内田2-11-4</p>
              <p class="note">「延床28坪で広々の家」「快適、外断熱工法の家」の2つのモデルを見学可能。</p>
              <a href="https://goo.gl/maps/PQK6uf4ai3T2" target="_blank" class="btn">Googleマップで見る</a>
            </div>
            <!-- /.company_model_house_item -->
          </div>
          <!-- /.company_model_house_item_wrap -->
        </div>
        <!-- /.company_model_house -->
      </div>
      <!-- /.company_aside -->
      <div class="company_contact">
        <p class="intro">展示場のご来場・お問い合わせは</p>
        <a class="tel" href="tel:0120-38-1601"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tel-gray.png" height="79" width="733" alt="0120-38-1601"></a>
        <p class="info">
          <span>（OPEN: 10:00〜18:00）</span>
          <span>定休日:第1・3・5火曜日、毎週水曜日</span>
        </p>
        <a href="<?php echo home_url('/')?>reserve/" class="btn">来場予約はこちら</a>
      </div>
      <!-- /.company_contact -->
    </div>
    <!-- /.company_contents_inner -->
  </div>
  <!-- /.company_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

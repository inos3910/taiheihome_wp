<?php
/**
* ------------------------//
* fileName : page-contact.php
* content : お問い合わせページ
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
//送信エラーの場合に格納されているSESSION変数を取得
session_start();
$error           = h($_SESSION['error']);
$contact_message = h($_SESSION['s_message']);
$user_name       = h($_SESSION['s_user_name']);
$user_name_kana  = h($_SESSION['s_user_name_kana']);
$user_email      = h($_SESSION['s_user_email']);
$postal_code     = h($_SESSION['s_postal_code']);
$user_address    = h($_SESSION['s_user_address']);
$user_tel        = h($_SESSION['s_user_tel']);

get_header();
?>
<div class="l_container">
  <div class="contact_contents">
    <div class="contact_title_wrap page_title_wrap">
      <h1 class="contact_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-contact.png" height="49" width="322" alt="CONTACT"><span>資料請求・お問い合わせ</span></h1>
    </div>
    <!-- /.contact_title_wrap -->
    <div class="contact_contents_inner">
      <?php if($error):?><p class="error"><?php echo $error;?></p><?php endif;?>
      <p class="contact_intro">資料請求・お問い合わせをご希望の方は、以下のフォームよりお気軽にお問い合わせ下さい。</p>
      <form action="<?php echo home_url('/')?>contact/mailsend/" method="post" class="contact_form" id="contact_form">
        <ul class="contact_form_list">
          <li>
            <div class="contact_form_item">
              <p class="label">ご希望の資料</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field contact_form_field_check">
              <div class="contact_form_field_box">
                <input type="checkbox" name="contact_check_01" id="contact_check_01" class="contact_check" value="当社についての資料" checked="checked">
                <label for="contact_check_01">当社についての資料</label>
              </div>
              <!-- /.contact_form_field_check.contact_form_field_box -->
              <div class="contact_form_field_box">
                <input type="checkbox" name="contact_check_02" id="contact_check_02" class="contact_check" value="家づくりについての資料">
                <label for="contact_check_02">家づくりについての資料</label>
              </div>
              <!-- /.contact_form_field_box -->
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">お問い合わせ内容</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <select name="contact_type" id="contact_type" class="contact_select">
                <option value="太平ホームの注文建築について">太平ホームの注文建築について</option>
                <option value="建築用地探しについて">建築用地探しについて</option>
                <option value="家づくりの資金計画について">家づくりの資金計画について</option>
                <option value="太平ホームの住宅展示場について">太平ホームの住宅展示場について</option>
                <option value="太平ホームのリフォームについて">太平ホームのリフォームについて</option>
                <option value="太平ホームの分譲住宅について">太平ホームの分譲住宅について</option>
                <option value="点検・メンテナンスのご依頼">点検・メンテナンスのご依頼</option>
                <option value="売却・土地活用のご相談">売却・土地活用のご相談</option>
                <option value="その他">その他</option>
              </select>
              <!-- /.contact_select -->
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">お問い合わせ詳細</p>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <textarea name="contact_message" id="contact_message" class="contact_message" placeholder="ここに内容を入力して下さい"><?php if($error){ echo $contact_message;}?></textarea>
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">お名前</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <input type="text" name="user_name" id="user_name" class="user_name" value="<?php if($error){ echo $user_name;}?>" autocomplete="name">
              <p class="note">例）太平 太郎</p>
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">ふりがな</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <input type="text" name="user_name_kana" id="user_name_kana" class="user_name_kana" value="<?php if($error){ echo $user_name_kana;}?>" placeholderたいへい たろう>
              <p class="note">例）たいへい たろう</p>
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">メールアドレス</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <input type="email" name="user_email" id="user_email" class="user_email" value="<?php if($error){ echo $user_email;}?>" placeholder="example@abc.jp" autocomplete="email">
              <p class="note">例）example@abc.jp</p>
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">ご住所</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <div class="contact_form_field_box">
                <span class="postal_icon">〒</span>
                <input type="text" name="postal-code" id="postal-code" class="postal-code" value="<?php if($error){ echo $postal_code;}?>" placeholder="000-0000" autocomplete="postal-code">
              </div>
              <!-- /.contact_form_field_box -->
              <div class="contact_form_field_box">
                <input type="text" name="user_address" id="user_address" class="user_address" value="<?php if($error){ echo $user_address;}?>" placeholder="埼玉県北葛飾郡杉戸町杉戸2-7-3">
              </div>
              <!-- /.contact_form_field_box -->
              <p class="note">例）埼玉県北葛飾郡杉戸町杉戸2-7-3</p>
            </div>
            <!-- /.contact_form_field -->
          </li>
          <li>
            <div class="contact_form_item">
              <p class="label">TEL</p>
              <span class="required">必須</span>
            </div>
            <!-- /.contact_form_item -->
            <div class="contact_form_field">
              <input type="tel" name="user_tel" id="user_tel" class="user_tel" value="<?php if($error){ echo $user_tel;}?>" placeholder="03-1234-5678" autocomplete="tel">
              <p class="note">例）03-1234-5678</p>
            </div>
            <!-- /.contact_form_field -->
          </li>
        </ul>
        <!-- /.contact_form_list -->
        <div class="contact_policy">
          <h2 class="contact_policy_caption">プライバシーポリシー</h2>
          <div class="contact_policy_inner">
            <p>太平ホーム株式会社（以下、「当社」といいます）は、お客様の個人情報がプライバシーを構成する重要な情報であることを認識し、当該情報の取扱いに最大限の注意を払うことは、当社の社会的責務であると考えております。当社のウェブサイトをご利用されるお客様は、個人情報を開示されなくてもほとんどのサービスをご利用することができます。また、当社のウェブサイトを参照しただけでは、お客様の個人情報が収集されることはありません。但し、特定のサービスにつきましては、お客様から個人情報をご提供して頂けない場合に、ご利用することができない可能性がありますのでご了解ください。当社が設置したウェブサーバーにおいて入手した個人情報の取扱いについては、以下のとおり「個人情報保護方針」を定め、全ての役員及び関係従業員に徹底を図っております。</p>
            <p>個人情報保護方針</p>
            <ul>
              <li>1.当社は、お客様の個人情報を取り扱っている部門単位で管理責任者を置き、個人情報の適切な管理に努めます。</li>
              <li>2.当社は、社内規程の整備、合理的なセキュリティ対策を行い、個人情報への不正な侵入、個人情報の紛失、改ざん、第三者への漏洩等の危険防止に努めます。</li>
              <li>
                <p>3.当社は、お客様からご提供頂いた個人情報を、以下のいずれかに該当する場合を除き、第三者に対して開示致しません。</p>
                <ul>
                  <li>・お客様の同意がある場合</li>
                  <li>・法令等により開示を要請された場合</li>
                  <li>・お客様が希望されるサービスを提供するうえで、当社が業務委託先に必要な範囲で開示する場合。この場合、当社は、当該業務委託先に対し、当社と同様適切な管理を行うよう契約により義務づけ、当該業務委託先からの個人情報の漏洩・再提供の防止などを図ります。</li>
                  <li>・お客様数統計的なデータ（例：男女別、年齢別お客様数等）としてお客様個人を識別できない状態に加工した場合。</li>
                </ul>
              </li>
              <li>4.お客様が、当社の管理するお客様ご自身の個人情報の照会、修正等を希望される場合には、当社までご連絡いただければ、当社は、合理的な範囲で速やかに対応させていただきます。</li>
              <li>5.当社は、お客様の個人情報の保護に関係する法令およびその他の規範を遵守するとともに、個人情報保護方針の内容を適宜見直し、その改善に努めます。</li>
            </ul>
            <p>クッキーの利用について</p>
            <p>当社のウェブサイトには、「cookie」情報を使用して提供されるサービスがあります。「cookie」は、お客様にサービスを提供するために、お客様が使用されているコンピュータを識別することが可能ですが、当該ウェブサイトにてお客様が個人情報を入力しない限り、お客様自身を特定することはできません。「cookie」を通して特定されるお客様の情報は当社のウェブサイトをより便利にご利用頂く目的のためだけに使用されます。また、当社サイトをご利用になった際に使用された「cookie」により、他のサイトに個人情報が流出する恐れはありません。お客様は、各々のブラウザにおいて「cookie」を無効にする設定を行えますが、その場合、ネットワーク上にて当社が提供するサービスのご利用頂ける範囲が限定されることがあります。</p>
          </div>
          <!-- /.contact_policy_inner -->
        </div>
        <!-- /.contact_form_policy -->
        <button id="confirm_btn" class="btn">確認画面へ</button>
      </form>
      <!-- /.contact_form -->
    </div>
    <!-- /.contact_contents_inner -->
  </div>
  <!-- /.contact_contents -->
</div>
<!--/.l_container-->
<?php
get_footer();
//セッション変数を破棄
$_SESSION = array();
session_destroy();
?>

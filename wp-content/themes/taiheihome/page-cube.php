<?php
/**
* ------------------------//
* fileName : page-cube.php
* content : T's ONEキューブページ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_detail_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.product_title_wrap.page_title_wrap -->
    <div class="product_detail_contents_inner">
      <div class="product_detail_visual cube_visual">
        <h2 class="product_detail_logo">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-cube-l.png" height="336" width="427" alt="T's ONEキューブ">
        </h2>
        <!-- /.product_detail_logo -->
      </div>
      <!-- /.product_detail_visual.cube_visual -->
      <div class="cube_concept">
        <div class="cube_concept_contents">
          <h2 class="product_concept_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept.png" height="38" width="183" alt="concept"></span>シンプルに<br>空間の質を追求。</h2>
          <p class="product_concept_text">明快なセレクトプロセス&amp;価格で、<br>のびやかな暮らし心地を実現する立方体（cube）フォルムの規格住宅。<br>こだわりの住品質と開放感あふれる大空間を叶えます。</p>
        </div>
        <!-- /.cube_concept_contents -->
        <div class="cube_concept_inner">
          <div class="cube_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-concept-01.jpg" height="554" width="592" alt="キューブ 正面外観">
          </div>
          <div class="cube_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-concept-02.jpg" height="554" width="590" alt="キューブ 外観">
          </div>
          <div class="cube_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-concept-03.jpg" height="554" width="592" alt="キューブ 斜め外観">
          </div>
        </div>
        <!-- /.cube_concept_inner -->
      </div>
      <!-- /.cube_concept -->
      <div class="product_concept_plan cube_concept_plan">
        <h2 class="product_concept_plan_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept-plan.png" height="44" width="298" alt="concept plan."></span>ミニマルな構成で、<br>想像以上の<br>開放感を生み出す。</h2>
        <div class="product_concept_plan_box cube_floor_01">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">勝手口付きの<br>洗面・脱衣スペース</h3>
              <p>洗面・脱衣スペースには、庭に洗濯物を干すときの動線に配慮して勝手口も設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">家具配置の自由度が高い<br>シンプルなLDK</h3>
              <p>21帖超のゆとりに満ちた団らん空間。家具レイアウトの自由度が高いスクエアな空間が、暮らしの可能性を広げます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">趣味の時間を満喫できる<br>ライブラリ＆ホビールーム</h3>
              <p>主寝室と子ども部屋のほかに、趣味の空間として活用できる居室を設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor cube_guide_01 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-all-01.jpg"  data-size="830x822">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-01.jpg" height="1013" width="1736" alt="キューブ 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">玄関&amp;洗面室&amp;LDKへ繋がる<br>３WAY動線のキッチン</h3>
              <p>家事効率に配慮し、洗面室だけでなく、玄関にもダイレクトに出入りできる３WAY動線をしつらえています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいをはぐくむ<br>リビング階段</h3>
              <p>ダイニングの目の前に、２階へとつながる階段をレイアウト。おでかけ時も、帰宅時も、親子の会話を自然とはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">ご家族みんなで共用できる<br>ファミリークローゼット</h3>
              <p>２階ホールには、ご家族みんなで共用できるファミリークローゼットを設計。季節家電や日用品などの整理にも役立ちます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.cube_floor_01 -->
        <div class="product_concept_plan_box cube_floor_02">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">玄関&amp;洗面室&amp;LDKへ繋がる<br>３WAY動線のキッチン</h3>
              <p>家事効率に配慮し、洗面室だけでなく、玄関にもダイレクトに出入りできる３WAY動線をしつらえています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいをはぐくむ<br>リビング階段</h3>
              <p>家事効率に配慮し、洗面室だけでなく、玄関にもダイレクトに出入りできる３WAY動線をしつらえています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">ご家族だけのミニシアター<br>になるひろびろAVルーム</h3>
              <p>２階に設計したAVルームは、ご家族だけのもうひとつの団らん空間。映画や音楽、読書を満喫していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor cube_guide_02 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-all-02.jpg"  data-size="830x829">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-02.jpg" height="1013" width="1728" alt="キューブ 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner cube_floor_bottom">
            <div class="product_concept_plan_item">
              <h3 class="caption">玄関と洗面室をつなぐ<br>スムーズなおでかけ動線</h3>
              <p>玄関から洗面室へとストレートにつながる動線を設計。おでかけ前の身だしなみチェックなどに便利です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">天気を気にせず洗濯できる<br>インナーバルコニー</h3>
              <p>雨の日でも洗濯物を干せるインナーバルコニーを設計。使い勝手がいいだけでなく、外観デザイン上のアクセントにもなります。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.cube_floor_02 -->
        <div class="product_concept_plan_box cube_floor_03">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">玄関&amp;洗面室&amp;LDKへ繋がる<br>３WAY動線のキッチン</h3>
              <p>スポーツ用品や雨具なども収められる、ゆとりある玄関収納を設計。玄関まわりをいつもすっきりと片付けられます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">いつも繋がりを感じられる<br>ワンフロアLDK</h3>
              <p>どこにいてもご家族のつながりを感じられる、間仕切りのない22帖超のLDK。団らんのひとときをのびやかに演出します。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">プライバシーに配慮して<br>２階に洗面所＆浴室を設計</h3>
              <p>洗面室＆浴室は、ゲストの目に触れにくい2階に設計。プライバシー空間とパブリック空間を明確に分けています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor cube_guide_03 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-all-03.jpg"  data-size="830x824">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-floor-03.jpg" height="1013" width="1733" alt="キューブ 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner cube_floor_bottom">
            <div class="product_concept_plan_item">
              <h3 class="caption">使い勝手に優れた<br>勝手口付きのキッチン</h3>
              <p>お料理中もご家族とふれあえる対面カウンターキッチンには、ゴミ出しなどに便利な勝手口をしつらえています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">空間をすっきりと整理できる<br>２階廊下物入れ</h3>
              <p>２階の廊下に大きな収納スペースを設計。扇風機などの季節家電、かさばりがちな日用品ストックの収納に重宝していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.cube_floor_03 -->
      </div>
      <!-- /.product_concept_plan.cube_concept_plan -->
      <div class="product_inner_space">
        <h2 class="product_inner_space_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-inner-space.png" height="40" width="271" alt="inner space"></span>光と風が、<br>シンプルな空間を<br>引き立てる。</h2>
        <div class="product_inner_space_grid gallery_group">
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-01.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-01.jpg" height="385" width="583" alt="リビング">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-02.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-02.jpg" height="385" width="583" alt="洋室">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-03.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-03.jpg" height="385" width="583" alt="子供部屋">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-04.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-04.jpg" height="385" width="583" alt="キッチン＆リビング・ダイニング">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-05.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-05.jpg" height="385" width="583" alt="カウンターキッチン">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-06.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/cube-inner-space-06.jpg" height="385" width="583" alt="階段下リビングスペース">
            </a>
          </figure>
        </div>
        <!-- /.product_inner_space_grid -->
        <?php get_template_part('block','btns'); ?>
      </div>
      <!-- /.cube_inner_space -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_list.product_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.product_detail_contents_inner -->
  </div>
  <!-- /.product_detail_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

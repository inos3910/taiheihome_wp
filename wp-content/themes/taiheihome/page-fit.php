<?php
/**
* ------------------------//
* fileName : page-fit.php
* content : T's ONEフィットページ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_detail_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.product_title_wrap.page_title_wrap -->
    <div class="product_detail_contents_inner">
      <div class="product_detail_visual fit_visual">
        <h2 class="product_detail_logo">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-fit-l.png" height="332" width="426" alt="T's ONEフィット">
        </h2>
        <!-- /.product_detail_logo -->
      </div>
      <!-- /.product_detail_visual.fit_visual -->
      <div class="fit_concept">
        <div class="fit_concept_contents">
          <h2 class="product_concept_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept.png" height="38" width="183" alt="concept"></span>自分らしさを<br>大切にする。</h2>
          <p class="product_concept_text">それは、自分らしく都市を謳歌するための住まい。<br>こだわりをぎっしりと詰め込み、お客様のご予算、お持ちの土地に<br>フィットすることを目指した規格住宅。</p>
        </div>
        <!-- /.fit_concept_contents -->
        <div class="fit_concept_inner">
          <div class="fit_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-concept-01.jpg" height="554" width="592" alt="フィット 正面外観">
          </div>
          <div class="fit_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-concept-02.jpg" height="554" width="592" alt="フィット 駐車スペース・外観">
          </div>
          <div class="fit_concept_pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-concept-03.jpg" height="554" width="592" alt="フィット 斜め外観">
          </div>
        </div>
        <!-- /.fit_concept_inner -->
      </div>
      <!-- /.fit_concept -->
      <div class="product_concept_plan fit_concept_plan">
        <h2 class="product_concept_plan_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept-plan.png" height="44" width="298" alt="concept plan."></span>さりげなく、<br>心地よく、<br>個性が輝く日々へ。</h2>
        <div class="product_concept_plan_box fit_floor_01">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいを<br>はぐくむリビング階段</h3>
              <p>ダイニングの目の前に、２階へとつながる階段をレイアウト。おでかけ時も、帰宅時も、親子の会話を自然とはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">家事の合間にひと息<br>つけるママコーナー</h3>
              <p>キッチンのすぐ隣にママのための空間を設計。レシピ本を読んだり、インターネットを楽しんだり、自由に活用していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">成長に合わせて<br>２室に分けられる子ども</h3>
              <p>お子様たちの成長に合わせ、将来的に２室に分けられるよう設計しています。お子様たちが独立した跡は、書斎や納戸にも。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor fit_guide_01 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-all-01.jpg"  data-size="830x814">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-01.jpg" height="1013" width="1734" alt="フィット 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">開放感あふれる<br>20帖超のひろびろLDK</h3>
              <p>ご家族団らんのメインステージとなるLDKには、20帖超のゆとりある空間を確保しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">お料理中も会話を楽しめる<br>対面カウンターキッチン</h3>
              <p>リビングでくつろぐご家族と会話しながらお料理できる、対面カウンターキッチン。充実の収納スペースも魅力です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">天気を気にせず洗濯できる<br>インナーバルコニー</h3>
              <p>雨の日でも洗濯物を干せるインナーバルコニーを設計。使い勝手がいいだけでなく、外観デザイン上のアクセントにもなります。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.fit_floor_01 -->
        <div class="product_concept_plan_box fit_floor_02">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">お料理中も会話を楽しめる<br>対面カウンターキッチン</h3>
              <p>リビングでくつろぐご家族と会話しながらお料理できる、対面カウンターキッチン。充実の収納スペースも魅力です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいをはぐくむ<br>リビング階段</h3>
              <p>ダイニングの目の前に、２階へとつながる階段をレイアウト。おでかけ時も、帰宅時も、親子の会話を自然とはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">趣味の空間として活用<br>できるフリースペース</h3>
              <p>ライフスタイルに応じて自由に活用できるフリースペースを設計。ファミリーライブラリーや趣味の空間として活用していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor fit_guide_02 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-all-02.jpg"  data-size="830x820">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-02.jpg" height="1013" width="1733" alt="フィット 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner fit_floor_bottom">
            <div class="product_concept_plan_item">
              <h3 class="caption">掃除機や日用品などの<br>収納に便利なリビング物入</h3>
              <p>掃除機などの家族共用アイテムをすっきりと収納できる、便利な物入れを設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">W.I.C.付きの<br>ゆとりに満ちた主寝室</h3>
              <p>主寝室には7.8帖のひろびろとした面積を確保するとともに、2帖超のゆとりあるW.I.C.も設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.fit_floor_02 -->
        <div class="product_concept_plan_box fit_floor_03">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">水まわりを近接させた<br>スムーズな家事動線</h3>
              <p>家事効率に配慮し、キッチン・洗面室・浴室を近接。すっきりと片付けられるよう、収納スペースも充実させています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">お料理中も会話を楽しめる<br>対面カウンターキッチン</h3>
              <p>リビングでくつろぐご家族と会話しながらお料理できる、対面カウンターキッチン。充実の収納スペースも魅力です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">W.I.C.付きの<br>ゆとりに満ちた主寝室</h3>
              <p>7帖超のひろびろとした主寝室には、ご夫婦の衣類をまとめて整理できる2.5帖のゆとりあるW.I.C.を設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor fit_guide_03 gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-all-03.jpg"  data-size="830x816">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-floor-03.jpg" height="1013" width="1733" alt="フィット 間取り図">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner fit_floor_bottom">
            <div class="product_concept_plan_item">
              <h3 class="caption">親子のふれあいを<br>はぐくむリビング階段</h3>
              <p>ダイニングの目の前に、２階へとつながる階段をレイアウト。おでかけ時も、帰宅時も、親子の会話を自然とはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">成長に合わせて２室に<br>分けられる子ども部屋</h3>
              <p>お子様たちの成長に合わせ、将来的に２室に分けられるよう設計しています。お子様たちが独立した後は、書斎や納戸にも。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box.fit_floor_03 -->
      </div>
      <!-- /.product_concept_plan.cube_concept_plan -->
      <div class="product_inner_space">
        <h2 class="product_inner_space_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-inner-space.png" height="40" width="271" alt="inner space"></span>さりげないこだわりが、<br>暮らしの歓びを広げる。</h2>
        <div class="product_inner_space_grid gallery_group">
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-01.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-01.jpg" height="385" width="583" alt="デスクスペース">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-02.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-02.jpg" height="385" width="583" alt="寝室">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-03.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-03.jpg" height="385" width="583" alt="子供部屋手前">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-04.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-04.jpg" height="385" width="583" alt="バルコニー">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-05.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-05.jpg" height="385" width="583" alt="子供部屋奥">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-06.jpg" data-size="585x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fit-inner-space-06.jpg" height="385" width="583" alt="リビングスペース">
            </a>
          </figure>
        </div>
        <!-- /.product_inner_space_grid -->
        <?php get_template_part('block','btns'); ?>
      </div>
      <!-- /.product_inner_space -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_list.product_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.product_detail_contents_inner -->
  </div>
  <!-- /.product_detail_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

<?php
/**
* ------------------------//
* fileName : page-hobby.php
* content : T's ZEROホビーページ
* last updated : 20160426
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_detail_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.product_title_wrap.page_title_wrap -->
    <div class="product_detail_contents_inner">
      <div class="product_detail_visual hobby_visual">
        <h2 class="product_detail_logo">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo-hobby-l.png" height="330" width="422" alt="T's ZERO +ホビー">
        </h2>
        <!-- /.product_detail_logo -->
      </div>
      <!-- /.product_detail_visual.hobby_visual -->
      <div class="hobby_concept">
        <div class="hobby_concept_inner">
          <div class="hobby_concept_contents">
            <h2 class="product_concept_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept.png" height="38" width="183" alt="concept"></span>遊びをいつも隣に。</h2>
            <p class="product_concept_text">クラシカのコンセプトに<br>「ホビースタイル」の要素を採り入れた注文建築。<br>DIYガレージやシアタールームなど、<br>アクティブに暮らしを愉しめる、<br>こだわりの空間を創出します。</p>
          </div>
          <!-- /.hobby_concept_contents -->
          <div class="hobby_concept_pic_01">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-concept-01.jpg" height="550" width="460" alt="ホビー バルコニー">
          </div>
          <!-- /.hobby_concept_pic_01 -->
          <div class="hobby_concept_pic_02">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-concept-02.jpg" height="550" width="460" alt="ホビー 外観">
          </div>
          <!-- /.hobby_concept_pic_02 -->
          <div class="hobby_concept_pic_03">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-concept-03.jpg" height="554" width="896" alt="ホビー オレンジの外壁">
          </div>
          <!-- /.hobby_concept_pic_03 -->
          <div class="hobby_concept_pic_04">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-concept-04.jpg" height="554" width="898" alt="ホビー 夕方ライトアップされた正面外観">
          </div>
          <!-- /.hobby_concept_pic_04 -->
        </div>
        <!-- /.hobby_concept_inner -->
      </div>
      <!-- /.hobby_concept -->
      <div class="product_concept_plan">
        <h2 class="product_concept_plan_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-concept-plan.png" height="44" width="298" alt="concept plan."></span>家族の時間も、<br>ひとりの時間も<br>愉しめるように。</h2>
        <div class="product_concept_plan_box">
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">ワンフロアをまるごと<br>見渡せるキッチン</h3>
              <p>ウッドデッキやインナーガレージまで見渡せる位置にキッチンをレイアウト。ご家族のふれあいを豊かにはぐくみます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">趣味に没頭できる<br>階段下の隠れ家スペース</h3>
              <p>階段下にデスクを造作した隠れ家スペースを設計。LDKにいるご家族とのつながりを感じながら、趣味に没頭できる空間です。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">ご夫婦の衣類をまとめて<br>収納できるW.I.C.</h3>
              <p>主寝室には3帖のゆとりあるウォークインクローゼットを設計。ご夫婦の衣類をまとめて収納できます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
          <div class="product_concept_plan_floor hobby_guide gallery_group">
            <figure class="modal">
              <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-floor-all.jpg"  data-size="847x833">
                <img class="floor" src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-floor.jpg" height="1013" width="1734" alt="ホビー 間取り">
              </a>
            </figure>
            <!-- /.modal -->
          </div>
          <!-- /.product_concept_plan_floor -->
          <div class="product_concept_plan_inner">
            <div class="product_concept_plan_item">
              <h3 class="caption">LDKと一体利用できる<br>ワイドなウッドデッキ</h3>
              <p>ご家族でBBQをしたり、友だちを招いてパーティを開いたり。休日の楽しみを広げるウッドデッキを設計しています。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">クルマいじりを愉しめる<br>ひろびろインナーガレージ</h3>
              <p>屋根の下で愛車を大切に保管できるインナーガレージを設計。DIYや自転車の整備などにも重宝していただけます。</p>
            </div>
            <!-- /.product_concept_plan_item -->
            <div class="product_concept_plan_item">
              <h3 class="caption">夜空を眺めながら<br>優雅に憩えるバルコニー</h3>
              <p>バルコニーには、もうひとつの居室として使えるほどの広さを確保。カウチを置けば、空の下で憩えるリゾート風空間に。</p>
            </div>
            <!-- /.product_concept_plan_item -->
          </div>
          <!-- /.product_concept_plan_inner -->
        </div>
        <!-- /.product_concept_plan_box -->
      </div>
      <!-- /.product_concept_plan -->
      <div class="product_inner_space hobby_inner_space">
        <h2 class="product_inner_space_title"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/product-title-inner-space.png" height="40" width="271" alt="inner space"></span>のびやかな発想が、<br>大人の遊び心を刺激する。</h2>
        <div class="product_inner_space_grid product_inner_space_grid_4 gallery_group">
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-01.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-01.jpg" height="385" width="421" alt="ホビースペース">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-02.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-02.jpg" height="385" width="421" alt="趣味の部屋">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-03.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-03.jpg" height="385" width="421" alt="ガーデニング">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-04.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-04.jpg" height="385" width="421" alt="ビリヤード">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-05.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-05.jpg" height="385" width="421" alt="暖炉">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-06.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-06.jpg" height="385" width="421" alt="読書部屋">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-07.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-07.jpg" height="385" width="421" alt="子供部屋">
            </a>
          </figure>
          <figure class="modal">
            <a href="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-08.jpg" data-size="421x385">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hobby-inner-space-08.jpg" height="385" width="421" alt="開放的な風呂">
            </a>
          </figure>
        </div>
        <!-- /.product_inner_space_grid -->
        <?php get_template_part('block','btns'); ?>
      </div>
      <!-- /.product_inner_space.hobby_inner_space -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_list.product_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.product_detail_contents_inner -->
  </div>
  <!-- /.product_detail_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

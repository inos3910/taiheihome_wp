<?php
/**
* ------------------------//
* fileName : page-product.php
* content : 商品一覧ページ
* last updated : 20160408
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="product_contents">
    <div class="product_title_wrap page_title_wrap">
      <h1 class="product_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-product.png" height="71" width="337" alt="PRODUCT"><span>商品紹介</span></h1>
    </div>
    <!-- /.page_title_wrap -->
    <ul class="product_list">
      <?php get_template_part('block','product'); ?>
    </ul>
    <!-- /.product_list -->
  </div>
  <!-- /.product_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

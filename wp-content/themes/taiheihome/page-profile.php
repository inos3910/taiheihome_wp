<?php
/**
* ------------------------//
* fileName : page-profile.php
* content : スタッフ紹介ページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="profile_contents">
    <div class="profile_title_wrap page_title_wrap">
      <h1 class="profile_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-profile.png" height="48" width="294" alt="PROFILE"><span>スタッフ紹介</span></h1>
    </div>
    <!-- /.page_title_wrap -->
    <ul class="profile_list">
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Shigeto-Kikuchi.jpg" height="630" width="468" alt="菊池 臣人">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">設計部部長／一級建築士・一級建築施工管理技士・CASBEE戸建評価員</p>
            <p class="profile_item_name">菊池 臣人<span>Shigeto Kikuchi</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>家づくりを始めるときに大切なのは、「必ずやりたいこと」「こだわり・希望」、そして「現在の住まいについて困っていること」を明確にすることです。それらを認識しながら進めれば、きっと満足のいくわが家ができあがるはずです！</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>ドライブ、読書、野球観戦</li>
                  <li><span>好きな映画・本：</span>『テルニエ・ロマエ』</li>
                  <li><span>尊敬する人：</span>宮脇檀（建築家）、大泉洋</li>
                  <li><span>１０年後の夢：</span>１０年後もお客様から頼んでよかったと言っていただけること</li>
                  <li><span>私が思ういい家：</span>住み心地がよく、「居場所」をつくることのできる家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Masaki-Kurihara.jpg" height="630" width="468" alt="栗原 正樹">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">営業課課長／住宅ローンアドバイザー</p>
            <p class="profile_item_name">栗原 正樹<span>Masaki Kurihara</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>家は大きな買い物です。また、「家族の命を守りたい」「暮らしを豊かにしたい」といった、さまざまな想いを込めてつくり上げるものでもあります。だから、お客様はもちろん、私たちも真剣です。お客様の立場に立って、精一杯、理想の家づくりをお手伝いいたします。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>野球、ゴルフ、スポーツ観戦　／　好きな映画・本：『マイ・ライフ』『レオン』</li>
                  <li><span>尊敬する人：</span>両親、徳川家康、高橋由伸</li>
                  <li><span>１０年後の夢：</span>人生を一生懸命生きる。実家を建て替える。ゴルフでパープレー。</li>
                  <li><span>私が思ういい家：</span>長く安心して過ごせる家、帰ると心がほっと落ち着く家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Kaoru-Watanabe.jpg" height="630" width="468" alt="渡邉 薫">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">営業課主任／宅地建物取引士・住宅ローンアドバイザー・住宅FPマスター二級</p>
            <p class="profile_item_name">渡邉 薫<span>Kaoru Watanabe</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>家づくりは、生涯における大仕事です。金額も大きいため、それに見合ったサービスと、末永くおつきあいさせていただくことが大切だと考えています。本音で相談していただける担当者として、お客様から信頼され、夢の実現をお手伝いできるよう努めています。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>散歩、写真、和太鼓、弓道、車、インテリア</li>
                  <li><span>尊敬する人：</span>平子繁会長</li>
                  <li><span>１０年後の夢：</span>健康第一で、自分たちで理想の家をつくり、仕事も家庭も大切にすること</li>
                  <li><span>私が思ういい家：</span>ひとりひとりが愛着を抱き、笑顔でいられえる場所がある家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Wataru-Ikeda.jpg" height="630" width="468" alt="池田 渉">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">営業課／住宅ローンアドバイザー・相続診断士</p>
            <p class="profile_item_name">池田 渉<span>Wataru Ikeda</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>インターネットで何でも情報が手に入る今、家づくりでもっとも大切なのは「emotion」すなわち感情だと思います。私たちは、お客様の感情をしっかりと受け止め、理想を具現化するために力を尽くしたいと考えています。あなたとお会いできることを楽しみにしています。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>私が思ういい家：</span>長く安心して過ごせる家、帰ると心がほっと落ち着く家</li>
                  <li><span>趣味・特技：</span>卓球、ブラジル音楽演奏</li>
                  <li><span>好きな映画・本：</span>『ゴジラ』（初代）</li>
                  <li><span>尊敬する人：</span>イチロー選手</li>
                  <li><span>１０年後の夢：</span>住宅営業を通して、自分もお客様も社会も「幸せ」を感じられる社会にすること</li>
                  <li><span>私が思ういい家：</span>「これが私の理想の家です！」と思える、居心地のいい家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Kenji-Furukawa.jpg" height="630" width="468" alt="古川 健司">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">営業課／二級建築士・宅地建物取引士</p>
            <p class="profile_item_name">古川 健司<span>Kenji Furukawa</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>「太平ホームで家を建ててよかった」と思ってもらえるよう心がけています。お客様にとっての身近な相談者・パートナーとして、夢のマイホームづくりを実現するためのお手伝いをさせていただきます。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>私が思ういい家：</span>住み心地がよく、「居場所」をつくることのできる家</li>
                  <li><span>趣味・特技：</span>読書、古本屋めぐり</li>
                  <li><span>好きな映画・本：</span>北方謙三、『水滸伝』</li>
                  <li><span>尊敬する人：</span>稲森和夫</li>
                  <li><span>１０年後の夢：</span>40代で決めたことを全うし、50代は後継者を育てたい</li>
                  <li><span>私が思ういい家：</span>家族がいつも笑顔でいられて、にぎやがで楽しい家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Nobuyuki-Sakasai.jpg" height="630" width="468" alt="逆井 伸幸">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">建築事業部部長・現場監督／二級建築士・一級建築施工管理技士</p>
            <p class="profile_item_name">逆井 信幸<span>Nobuyuki Sakasai</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>大切なのは、自分の生活習慣に合った家をつくることです。私のおすすめは、壁紙などの色はできるだけシンプルにして家具やカーテンで色を付けるという手法です。これから家づくりを始める方は、いろいろと不安も抱えていると思います。ぜひお気軽にご相談ください。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>旅行、ドライブ、ゴルフ</li>
                  <li><span>好きな映画・本：</span>スタジオジブリの映画</li>
                  <li><span>尊敬する人：</span>本田宗一郎</li>
                  <li><span>１０年後の夢：</span>海外旅行</li>
                  <li><span>私が思ういい家：</span>住んでいる人の生活習慣に合っている家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Shinichi-Watanabe.jpg" height="630" width="468" alt="渡邊 真一">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">建築事業部次長・メンテナンスセンターセンター長／<br>二級建築施工管理技士・二級塗装技能士・増改築相談員</p>
            <p class="profile_item_name">渡邊 真一<span>Shinichi Watanabe</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>当社の家は、お引き渡し後のアフターメンテナンスに非常に力を入れて取り組んでいます。展示場にメンテナンスセンターも併設していますので、入居後も安心してお住まいいただけると思います。いつまでも末永いおつきあいをよろしくお願いいたします。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>料理、ゴルフ</li>
                  <li><span>好きな映画・本：</span>スペクタクル映画（ブレイブハート、グラディエーターなど）</li>
                  <li><span>尊敬する人：</span>坪内寿夫</li>
                  <li><span>１０年後の夢：</span>豪華客船クルーズ</li>
                  <li><span>私が思ういい家：</span>夏すずしく冬あたたかい、ランニングコストの少ない家 </li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Hiroaki-Shiraishi.jpg" height="630" width="468" alt="白石 浩明">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">建築事業部課長／二級建築士・二級土木施工管理技士</p>
            <p class="profile_item_name">白石 浩明<span>Hiroaki Shiraishi</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>一生に一度の家づくりですから、建てて終わりではなく、アフターサービス・メンテナンスにもしっかりと力を注ぐことのできる住宅会社・営業マンを選んでいただきたいと思います。その上で、私たちに理想の家づくりのお手伝いをさせていただければ幸いです。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>ゴルフ</li>
                  <li><span>尊敬する人：</span>父親</li>
                  <li><span>１０年後の夢：</span>健康でゴルフができていれば最高です</li>
                  <li><span>私が思ういい家：</span>使いやすい家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Hiroaki-Sato.jpg" height="630" width="468" alt="佐藤 大哲">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">建築事業部課長・現場監督</p>
            <p class="profile_item_name">佐藤 大哲<span>Hiroaki Sato</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>新築工事をはじめ、大小さまざまなリフォーム工事の施工管理を担当しています。お客様に満足していただけるよう工事品質の向上・管理を追求しているほか、工事後も不具合やご相談などがあれば気軽に声を掛けていただけるような信頼関係の構築を目指しています。</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>趣味・特技：</span>ドライブ</li>
                  <li><span>好きな映画・本：</span>『美味しんぼ』</li>
                  <li><span>尊敬する人：</span>野口英世</li>
                  <li><span>１０年後の夢：</span>自分で設計して仕様を決め、自分で施工管理をすること</li>
                  <li><span>私が思ういい家：</span>家族が健康で安心して暮らすことのできる家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
      <li class="profile_item">
        <div class="profile_item_pic">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/profile-Yoshinari-Kawanabe.jpg" height="630" width="468" alt="川鍋 圭也">
        </div>
        <!-- /.profile_pic -->
        <div class="profile_item_content">
          <div class="profile_item_head">
            <p class="profile_position">建築事業部・現場監督</p>
            <p class="profile_item_name">川鍋 圭也<span>Yoshinari Kawanabe</span></p>
          </div>
          <!-- /.profile_item_head -->
          <div class="profile_item_text">
            <p>間取り、収納、眺め、視線、材質、日当たり、内装、設備などなど。家づくりにはいろいろな条件がありますが、お客様にとって欠かすことのできない条件とは何でしょうか？　夢のマイホームは、これを考えることから始まります。一緒にいい家づくりを目指しましょう！</p>
            <dl>
              <dt><span>Personality</span></dt>
              <dd>
                <ul>
                  <li><span>私が思ういい家：</span>長く安心して過ごせる家、帰ると心がほっと落ち着く家</li>
                  <li><span>趣味・特技：</span>模写</li>
                  <li><span>好きな映画・本：</span>『シックスセンス』『カラスの親指』</li>
                  <li><span>尊敬する人：</span>伊能忠敬</li>
                  <li><span>１０年後の夢：</span>日本一周</li>
                  <li><span>私が思ういい家：</span>心の安らぎと癒やしを与えてくれる、ぬくもりのある家</li>
                </ul>
              </dd>
            </dl>
          </div>
          <!-- /.profile_item_text -->
        </div>
        <!-- /.profile_item_content -->
      </li>
      <!-- /.profile_item -->
    </ul>
    <!-- /.profile_list -->
  </div>
  <!-- /.profile_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

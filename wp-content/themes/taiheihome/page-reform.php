<?php
/**
* ------------------------//
* fileName : page-reform.php
* content : リフォームページ
* last updated : 20160418
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="reform_contents">
    <div class="reform_title_wrap page_title_wrap">
      <h1 class="reform_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-reform.png" height="51" width="259" alt="REFORM"><span>リフォーム</span></h1>
    </div>
    <!-- /.reform_title_wrap.page_title_wrap -->
    <div class="reform_contents_inner">
      <div class="reform_top">
        <h2 class="caption">ニーズに合わせた<br>いろいろなリフォーム<br>をご提案します。</h2>
        <p>時が経てば家にも家族にもいろいろな変化が生まれます。<br>家族構成や暮らし方の変化に合わせたより快適な生活を、リフォームで実現しませんか。<br>趣味を楽しむため、家族やペットのため、暮らし方の変化に対応するため、家を美しく保つためなど、<br>お客様の実現したいことをなんでもお聞かせ下さい。</p>
      </div>
      <!-- /.reform_top -->
      <div class="reform_middle">
        <div class="reform_middle_inner">
          <div class="text">
            <h3 class="caption">設備の入れ替えなどの便利リフォーム</h3>
            <p>わが家への愛着はそのままに、古くなった水まわり設備などを新しいものに交換し、新しい暮らし心地をお届けします。ゼロから家をつくることのできる豊富なノウハウを活かし、お客様のニーズに適切かつ迅速に対応いたします。</p>
          </div>
          <!-- /.text -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reform-pic-01.jpg" height="369" width="617" alt="外壁 洗面所 床 キッチン">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.reform_middle_inner -->
        <div class="reform_middle_inner">
          <div class="text">
            <h3 class="caption">一棟まるごと古リフォーム<br>「リバイバルハウス」</h3>
            <p>「リバイバルハウス」は、建替えの約半分の費用で家の外も中も新築そっくりに新生する、太平ホーム独自のリフォームプランです。ご希望・ご予算に応じて、さらなるグレードアップ工事を付け加えることもできます。</p>
          </div>
          <!-- /.text -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reform-pic-02.jpg" height="369" width="617" alt="リバイバルハウス">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.reform_middle_inner -->
        <div class="reform_middle_inner">
          <div class="text">
            <h3 class="caption">増改築リフォーム</h3>
            <p>居室の数を変更したり、２世帯住宅にしたり。ご家族構成やライフスタイルの変化に応じた、増改築リフォームも承ります。規模の大小は問わず、一件一件丁寧に対応いたしますので、ぜひお気軽にご相談ください。</p>
          </div>
          <!-- /.text -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/reform-pic-03.jpg" height="369" width="617" alt="間取りの立体模型">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.reform_middle_inner -->
        <div class="reform_middle_inner">
          <h3 class="caption">リフォーム保証システム</h3>
          <p>工事内容に応じて最大10年間保証します。 ※請負金額100万円（税別）以上のリフォーム工事対象</p>
          <div class="reform_middle_table">
            <div class="reform_middle_table_inner">
              <table class="table_01">
                <tbody>
                  <tr>
                    <th colspan="2">工事内容（リフォーム工事を行った箇所）</th>
                  </tr>
                  <tr>
                    <td rowspan="2">
                      <p>①構造耐力上主要な部分<span>※</span></p>
                      <p>②雨水の浸入を防止する部分</p>
                    </td>
                    <td>増築工事に伴い<br>新設した部分<span>※</span></td>
                  </tr>
                  <tr>
                    <td>既存のものに<br>改修を加えた部分</td>
                  </tr>
                  <tr>
                    <td colspan="2">①、②以外の部分</td>
                  </tr>
                </tbody>
              </table>
              <table class="table_02">
                <tr>
                  <th>保証期間</th>
                </tr>
                <tr>
                  <td>10年間</td>
                </tr>
                <tr>
                  <td>5年間</td>
                </tr>
                <tr>
                  <td class="last">保証項目ごとに※1年、3年、5年のいずれかの期間</td>
                </tr>
              </table>
            </div>
            <!-- /.reform_middle_table_inner -->
            <span class="note">※構造耐力上主要な部分および雨水の浸入を防止する部分の不具合の補修については、<br>住宅の品質確保の促進等に関する法律第70条に基づく「住宅の紛争処理の参考となるべき技術基準」に基づき判断致します。</span>
            <span class="note">※当リフォーム保証システムにおいて、増築とは、既存住宅の基礎外周部の外側において基礎の新設を行う工事をさすものとします。</span>
            <span class="note">※保証項目ごとの保証期間は、保証書に記載されている保証基準をご参照ください。</span>
          </div>
          <!-- /.reform_middle_table -->
        </div>
        <!-- /.reform_middle_table -->
      </div>
      <!-- /.reform_middle -->
      <div class="reform_bottom">
        <p>ご相談・お見積りはもちろん無料です。リフォームに関する各種補助金申請も承ります。</p>
        <h4 class="caption">リフォームに関するご相談は</h4>
        <div class="reform_link_wrap">
          <div class="reform_link_item">
            <a href="<?php home_url('/')?>contact/" class="btn">お問い合わせフォーム</a>
          </div>
          <!-- /.reform_link_item -->
          <div class="reform_link_item">
            <a href="tel:0120-38-1601" class="tel"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tel-gray.png" height="79" width="733" alt="0120-38-1601"></a>
            <span>（OPEN: 10:00〜18:00）</span>
            <span>定休日:第1・3・5火曜日、毎週水曜日</span>
          </div>
          <!-- /.reform_link_item -->
        </div>
        <!-- /.reform_link_wrap -->
      </div>
      <!-- /.reform_bottom -->
    </div>
    <!-- /.reform_contents_inner -->
  </div>
  <!-- /.reform_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

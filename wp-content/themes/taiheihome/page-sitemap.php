<?php
/**
* ------------------------//
* fileName : page-sitemap.php
* content : サイトマップページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="sitemap_contents">
    <div class="sitemap_title_wrap page_title_wrap">
      <h1 class="sitemap_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-sitemap.png" height="50" width="309" alt="SITE MAP"><span>サイトマップ</span></h1>
    </div>
    <!-- /.sitemap_title_wrap.page_title_wrap -->
    <?php get_template_part('block','sitemap'); ?>
  </div>
  <!-- /.sitemap_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

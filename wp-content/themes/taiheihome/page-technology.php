<?php
/**
* ------------------------//
* fileName : page-technology.php
* content : 構造・性能ページ
* last updated : 20160418
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="tech_contents">
    <div class="tech_title_wrap page_title_wrap">
      <h1 class="tech_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-technology.png" height="48" width="468" alt="TECHNOLOGY"><span>構造・性能</span></h1>
    </div>
    <!-- /.page_title_wrap -->
    <div class="tech_contents_inner">
      <div class="tech_top">
        <h2 class="caption">家に「強さ」と<br>「快適」と<br>「きれいな空気」を。</h2>
        <p>ご家族のかけがえのない日々を、いつまでも支え続けられる家をつくりたい。<br>そのような想いのもと、太平ホームは家の基本性能にどこまでもこだわっています。<br>見えない部分のクオリティが、誇り高い暮らし心地を支えています。</p>
      </div>
      <!-- /.tech_top -->
      <div class="tech_main">
        <div class="tech_group tech_group_01">
          <div class="tech_item">
            <h3 class="tech_item_heading">耐震性　|　木造軸組構法</h3>
            <h4 class="tech_item_caption">強い骨格と、間取りの柔軟性を求めて</h4>
            <div class="tech_item_inner">
              <p>太平ホームの家の構造には、木材を使用した土台・梁・柱で建物を組み上げる日本の伝統的工法「木造軸組工法」を採用しています。木材特有のしなやかさと強度を活かした耐震性・耐久性と、敷地条件に柔軟に対応できる設計自由度の高さ、また開口部を大きく取れるところも特長です。</p>
              <div class="pic">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tech-pic-01.jpg" height="361" width="295" alt="耐震性 木造軸組構法">
              </div>
              <!-- /.pic -->
            </div>
            <!-- /.tech_item_inner -->
          </div>
          <!-- /.tech_item -->
          <div class="tech_item">
            <h3 class="tech_item_heading">制振制</h3>
            <h4 class="tech_item_caption">繰り返しの地震から躯体を守ります。</h4>
            <div class="tech_item_inner">
              <p>自動車用ダンパー技術を応用した制振装置を構造躯体内に設置。地震による建物への変形エネルギーを吸収して構造躯体の損傷を軽減し、繰り返し襲ってくる余震にも耐え得る家を築きます。太平ホームは、万一の地震の際もしっかりとご家族を守れる家づくりを追求しています。</p>
              <div class="pic">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tech-pic-02.jpg" height="361" width="295" alt="制振制">
              </div>
              <!-- /.pic -->
            </div>
            <!-- /.tech_item_inner -->
          </div>
          <!-- /.tech_item -->
        </div>
        <!-- /.tech_group_01 -->
        <div class="tech_group tech_group_02">
          <div class="tech_item">
            <h3 class="tech_item_heading">断熱性　|　高性能樹脂窓</h3>
            <h4 class="tech_item_caption">家の中の居心地を変える大きな存在。</h4>
            <div class="tech_item_inner">
              <p>２枚のガラスの間に熱の出入りを軽減するアルゴンガスを封入、さらに内側を金属でコーティングし、単板ガラスの約4倍の断熱効果を発揮する「Low-E複層ガラス」を採用。また、結露が発生しにくい樹脂製サッシを採用。夏涼しく冬あたたかな室内環境を実現し、結露を防いで家の長寿命化に貢献します。</p>
              <div class="pic">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tech-pic-03.jpg" height="361" width="295" alt="断熱性 高性能樹脂窓">
              </div>
              <!-- /.pic -->
            </div>
            <!-- /.tech_item_inner -->
          </div>
          <!-- /.tech_item -->
          <div class="tech_item">
            <h3 class="tech_item_heading">空気の質</h3>
            <h4 class="tech_item_caption">家族の健康を守るきれいな空気を。</h4>
            <div class="tech_item_inner">
              <p>壁や天井には、石こうボードにシックハウス対策機能をプラスした「ハイクリンボード」を採用しています。ホルムアルデヒドを吸収分解し、室内の空気をクリーンで健やかにします。新築時にありがちないやな匂いを軽減するというメリットもあります。</p>
              <div class="pic">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tech-pic-04.jpg" height="361" width="295" alt="空気の質">
              </div>
              <!-- /.pic -->
            </div>
            <!-- /.tech_item_inner -->
          </div>
          <!-- /.tech_item -->
        </div>
        <!-- /.tech_group_02 -->
        <div class="tech_item_bottom">
          <div class="tech_item">
            <h3 class="tech_item_heading">断熱性　|　断熱材</h3>
            <h4 class="tech_item_caption">一年を通じて快適な家を目指して。</h4>
            <p>外気に接する天井・壁・床には高性能グラスウール「アクリア」を標準装備。結露に強く、燃えにくく、長寿命の「アクリア」をぎっしりと詰め込むことで冷暖房の効きが良く、夏涼しく冬あたたかな室内環境を実現しています。</p>
          </div>
          <!-- /.tech_item -->
          <div class="pic">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tech-pic-05.jpg" height="291" width="717" alt="断熱性 断熱材">
          </div>
          <!-- /.pic -->
        </div>
        <!-- /.tech_item_bottom -->
      </div>
      <!-- /.tech_main -->
      <div class="product_others">
        <h2 class="product_others_title">その他の商品一覧</h2>
        <ul class="product_others_list tech_others_list">
          <?php get_template_part('block','product'); ?>
        </ul>
        <!-- /.product_others_list.tech_others_list -->
      </div>
      <!-- /.product_others -->
    </div>
    <!-- /.tech_contents_inner -->
  </div>
  <!-- /.tech_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

<?php
/**
* ------------------------//
* fileName : page-works.php
* content : 施工実績一覧ページ
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="works_contents">
    <div class="works_title_wrap page_title_wrap">
      <h1 class="works_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-works.png" height="48" width="249" alt="WORKS"><span>施工実績一覧</span></h1>
    </div>
    <!-- /.page_title_wrap -->
    <ul class="works_list">
      <?php
      $count = 0;
      $works_args = array( 
        'post_type' => 'works',
        'posts_per_page' => 12,
        'orderby' => 'rand',
        );
      $works_query = new WP_Query( $works_args );
      $all_posts = $works_query->found_posts;
      if ( $works_query->have_posts() ) :
        while ( $works_query->have_posts() ) : $works_query->the_post();
        //ループ開始***************************************************
        //ページIDを変数に入れる
      $exclude[] = $post->ID;
      ?>
      <li<?php if($count===0):?> data-all="<?php echo $all_posts;?>"<?php endif;?> data-ex="<?php the_ID();?>">
      <a href="<?php the_permalink();?>"><?php $image = get_post_meta($post->ID, 'cf_works_slide', true); echo wp_get_attachment_image($image, 'medium'); ?></a>
      <p><?php the_title();?></p>
    </li>
    <?php
    //記事カウント
    $count++;
    //ループ終了************************************************
    endwhile;
    endif;
    wp_reset_postdata();
    ?>
  </ul>
  <!-- /.works_list -->
  <div class="works_more_btn"><button id="more" class="btn">さらに読み込む</button></div>
</div>
<!-- /.works_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

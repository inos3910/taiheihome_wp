<?php
/**
* ------------------------//
* fileName : single-post.php
* content : ブログ詳細ページ
* last updated : 20160411
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="post_contents">
    <div class="news_title_wrap page_title_wrap">
      <h1 class="news_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-blog.png" height="48" width="175" alt="BLOG"><span>ブログ記事</span></h1>
    </div>
    <!-- /.news_title_wrap.page_title_wrap -->
    <div class="post_contents_inner">
      <div class="post_contents_col">
        <div class="post_main">
          <?php if (have_posts()) : while (have_posts()) : the_post();
          //ループ開始************************************//
          ?>
          <div class="post_main_inner">
            <div class="post_date">
              <p>
                <span class="date"><?php echo get_post_time('d M')?></span>
                <span class="year"><?php the_time('Y')?></span>
              </p>
            </div>
            <!-- /.post_date -->
            <h2 class="post_main_title"><?php the_title();?></h2>
            <div class="post_main_contents">
              <?php if ( has_post_thumbnail() ) { the_post_thumbnail('full'); }?>
              <?php the_content();?>
            </div>
            <!-- /.post_main_contents -->
          </div>
          <!-- /.post_main_inner -->
          <div class="post_pagination">
            <?php previous_post_link('%link', '&laquo; 前の記事へ'); ?>
            <?php next_post_link('%link', '次の記事へ &raquo;'); ?>
          </div>
          <!-- /.post_pagenation.pagination -->
          <?php //ループ終了*******************************//
          endwhile;
          endif;
          ?>
        </div>
        <!-- /.post_main -->
        <div class="post_side">
          <div class="post_side_item">
            <h3 class="post_side_caption">最新の投稿</h3>
            <ul class="post_side_new_list">
              <?php
              global $news_query;
              $news_args = array( 
                'post_type' => 'post',
                'posts_per_page' => 5,
                'order' => 'DESC',
                'orderby' => 'date'
                );
              $news_query = new WP_Query( $news_args );
              if ( $news_query->have_posts() ) :
                while ( $news_query->have_posts() ) : $news_query->the_post();
                //ループ開始****************************************************
              ?>
              <li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
              <?php //ループ終了************************************************
              endwhile;
              endif;
              wp_reset_postdata();
              ?>
            </ul>
            <!-- /.post_side_new_list -->
          </div>
          <!-- /.post_side_item -->
          <div class="post_side_item">
            <h3 class="post_side_caption">アーカイブ</h3>
            <?php
            $year_prev = null;
            $months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month ,
              YEAR( post_date ) AS year,
              COUNT( id ) as post_count FROM $wpdb->posts
              WHERE post_status = 'publish' and post_date <= now( )
              and post_type = 'post'
              GROUP BY month , year
              ORDER BY post_date DESC");
            foreach($months as $month) :
              $year_current = $month->year;
            if ($year_current != $year_prev){
              if ($year_prev != null){?>
            </ul></div>
            <?php } ?>
            <p class="post_archive_year"><?php echo $month->year; ?>年</p>
            <ul class="post_archive_list">
              <?php } ?>
              <li>
                <a href="<?php bloginfo('url') ?>/date/<?php echo $month->year; ?>/<?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>">
                  <?php echo date("n", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>月
                  (<?php echo $month->post_count; ?>)
                </a>
              </li>
              <?php $year_prev = $year_current;
              endforeach; ?>
            </ul>
            <!-- /.post_archive_list -->
          </div>
          <!-- /.post_side_item -->
        </div>
        <!-- /.post_side -->
      </div>
      <!-- /.post_contents_col -->
    </div>
    <!-- /.post_contents_inner -->
  </div>
  <!-- /.post_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>

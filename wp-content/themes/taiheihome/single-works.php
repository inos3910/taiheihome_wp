<?php
/**
* ------------------------//
* fileName : single-works.php
* content : 加工実績詳細ページ
* last updated : 20160412
* version : 1.0
* ------------------------//
**/
get_header();
?>
<div class="l_container">
  <div class="works_post_contents">
    <div class="works_title_wrap page_title_wrap">
      <h1 class="works_title page_title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/title-works.png" height="48" width="249" alt="WORKS"><span>施工実績</span></h1>
    </div>
    <!-- /.works_title_wrap.page_title_wrap -->
    <div class="works_post_contents_inner">
      <?php if (have_posts()) : while (have_posts()) : the_post();
      //ループ開始************************************//
      ?>
      <h2 class="works_post_title"><?php the_title();?></h2>
      <div class="works_post_contents_col">
        <div class="works_post_main">
          <div class="works_post_carousel" id="carousel">
            <ul class="sp-slides">
              <?php
              $slider = SCF::get( 'cf_works_slide_group' );
              foreach ( $slider as $field_name => $field_value ) {?>
              <li class="sp-slide">
                <?php
                $attachment_id = $field_value['cf_works_slide'];
                $size = "medium";
                $image = wp_get_attachment_image_src( $attachment_id, $size );
                $attachment = get_post($attachment_id);
                $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
                $image_title = $attachment->post_title;
                ?>
                <img class="sp-image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $alt; ?>" title="<?php echo $image_title; ?>" />
              </li>
              <?php } ?>
            </ul>
            <!-- /.sp-slides -->
            <div class="sp-thumbnails">
              <?php
              foreach ( $slider as $field_name => $field_value ) {?>
              <?php
              $attachment_id = $field_value['cf_works_slide'];
              $size = "medium";
              $image = wp_get_attachment_image_src( $attachment_id, $size );
              $attachment = get_post($attachment_id);
              $alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
              $image_title = $attachment->post_title;
              ?>
              <img class="sp-thumbnail" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $alt; ?>" title="<?php echo $image_title; ?>" />
              <?php } ?>
            </div>
          </div>
          <!-- /.works_post_carousel -->
          <div class="works_main_contents">
            <?php the_content();?>
          </div>
          <!-- /.works_main_contents -->
          <div class="works_post_main_table">
            <dl class="works_post_main_table_row">
              <dt>建物種別</dt>
              <dd><?php echo get_post_meta($post->ID , 'cf_works_type' ,true); ?></dd>
            </dl>
            <dl class="works_post_main_table_row">
              <dt>所在地</dt>
              <dd><?php echo get_post_meta($post->ID , 'cf_works_place' ,true); ?></dd>
            </dl>
            <dl class="works_post_main_table_row">
              <dt>家族構成</dt>
              <dd><?php echo get_post_meta($post->ID , 'cf_works_family' ,true); ?></dd>
            </dl>
            <dl class="works_post_main_table_row">
              <dt>敷地面積</dt>
              <dd><?php echo get_post_meta($post->ID , 'cf_works_site_area' ,true); ?></dd>
            </dl>
            <dl class="works_post_main_table_row">
              <dt>延床面積</dt>
              <dd><?php echo get_post_meta($post->ID , 'cf_works_total_area' ,true); ?></dd>
            </dl>
          </div>
          <!-- /.works_main_table -->
        </div>
        <!-- /.works_post_main -->
        <div class="works_post_side">
          <h3 class="works_post_side_caption">その他の実績</h3>
          <ul class="works_post_side_list">
            <?php
            //現在の記事を除外する
            $exclude = array();
            $exclude[] = $post->ID;
            global $works_query;
            $works_args = array( 
              'post_type' => 'works',
              'posts_per_page' => 4,
              'orderby' => 'rand',
              'post__not_in' => $exclude,
              );
            $works_query = new WP_Query( $works_args );
            if ( $works_query->have_posts() ) :
              while ( $works_query->have_posts() ) : $works_query->the_post();
                //ループ開始*************************************************
            ?>
            <li>
              <a href="<?php the_permalink();?>"><?php $image = get_post_meta($post->ID, 'cf_works_slide', true); echo wp_get_attachment_image($image, 'medium'); ?></a>
              <p><?php the_title();?></p>
            </li>
            <?php //ループ終了************************************************
            endwhile;
            endif;
            wp_reset_postdata();
            ?>
          </ul>
          <!-- /.works_post_side_list -->
        </div>
        <!-- /.works_post_side -->
      </div>
      <!-- /.works_post_contents_col -->
      <a href="<?php echo home_url('/')?>works/" class="btn">施工実績一覧に戻る</a>
      <?php //ループ終了************************************************
      endwhile;
      endif;
      wp_reset_postdata();
      ?>
    </div>
    <!-- /.works_post_contents_inner -->
  </div>
  <!-- /.works_post_contents -->
</div>
<!--/.l_container-->
<?php get_footer(); ?>